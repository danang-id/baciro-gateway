# Baciro Gateway Front-End version 2.0.0

***[baciro-gateway](https://github.com/danang-id/baciro-gateway)*** is the front-end version of Baciro Gateway by PT DUA EMPAT TUJUH.

**IMPORTANT!**
1. Please read the list of [known issues](#known-issues) before procceding.
2. To download latest build of the application, visit the [latest release](https://github.com/danang-id/baciro-gateway/releases/latest). This build can be loaded to a web server directly, with REST back-end server addressed to `http://baciro.dev`. 
3. **This is a new version of Application using React 16.0.0 framework.** ***I really prefer you to choose this version instead the older one because of the features that the framework brings.*** However, if you'd like the *unfinished* PHP (CodeIgniter framework) version of the application, it is available on [codeigniter](https://github.com/danang-id/baciro-gateway/tree/codeigniter) branch.

## List of Contents

* [Getting Started](#getting-started)
  * [Requirements](#requirements)
  * [Installation](#installation)
* [Usage](#usage)
  * [On Development Stage](#on-development-stage)
  * [Building the TypeScript](#building-the-typescript)
  * [Create a Production Build](#create-a-production-build)
* [Documentation](#documentation)
* [Known Issues](#known-issues)
* [Built With](#built-with)
* [Contribution](#contribution)
* [Versioning](#versioning)
* [Authors](#authors)
* [License](#license)

## Getting Started

### Requirements

To use or deploy this application, make sure you have already install `nodejs` alongside with `npm`. Take time on [NodeJS Home Page](https://nodejs.org/) if you have not installed one.

### Installation

First clone this application project.

```bash
git clone https://github.com/danang-id/baciro-gateway.git
```

And then install all required node modules for this application.

```bash
cd baciro-gateway
npm install
```

## Usage

As ***Baciro Gateway*** Front-End is using [React](https://github.com/facebook/react) and developed upon [create-react-app](https://github.com/facebookincubator/create-react-app), use `react-scripts` within `npm` command to develop and deploy the application.

### On Development Stage

To runs the application in the development mode, first make sure configuration of dotenv files (`.env` and `.env.development`) are correct, especially on `HOST` and `HTTPS` setting.

Then run this on console.

```bash
npm start
```

Open `http://localhost:3000` to view it in the browser (or depends on `HOST` setting in dotenv files).

The page will automatically reload if you make edits. You will also see any lint errors in the console. 

### Building the TypeScript

***IMPORTANT***: **This application was built using TypeScript and TypeScript-React, so to make sure you edit the TypeScript or TypeScript-React files rather than the JavaScript one.** If you have no idea about what is TypeScript, please refer to [TypeScript home page](https://www.typescriptlang.org/).

**If TypeScript has not been installed on your system, you will not be able to run `tsc` command.** There are 2 ways to install TypeScript depending on how will you build the system.
1. Using console/terminal.
2. Using editor like `Sublime Text`.

First, install TypeScript by using `npm` globally so it's accessible through console/terminal. 

```bash
npm install --global typescript
```

Then, if you also want to use the build feature on Sublime Text, install [TypeScript plug-in for Sublime Text](https://packagecontrol.io/packages/TypeScript) or follow [this guideline](https://cmatskas.com/getting-started-with-typescript-and-sublime-text/).

*To build TypeScript-React files (.tsx)*
1. Using Console / Terminal

   ```bash
   tsc script.tsx --target ES6 --jsx react
   ```
2. Using Sublime Text
   + Open the `.tsx` file.
   + Press `Ctrl + B`.
   + Add this to parameter: `--target ES6 --jsx react` and press Enter.
   + Build finished.

*However, on generic TypeScript files (.ts), just use ES6 targeting parameter*
1. Using Console / Terminal

   ```bash
   tsc script.ts --target ES6
   ```
2. Using Sublime Text
   + Open the `.tsx` file.
   + Press `Ctrl + B`.
   + Add this to parameter: `--target ES6` and press Enter.
   + Build finished.

*You may set TypeScript compiler to watch over changes in TypeScript files by adding `--watch` parameter upon compiling.*
1. Using Console / Terminal

   ```bash
   tsc script.tsx --target ES6 --jsx react --watch
   ```
2. Using Sublime Text
   + Open the `.tsx` file.
   + Press `Ctrl + B`.
   + Add this to parameter: `--target ES6 --jsx react  --watch` and press Enter.
   + Build finished.

**Note**: React compiler (the `npm start` command) will **NOT** auto watching change over the TypeScript or TypeScript-React files. However, once you compile the TypeScript or TypeScript-React files to JavaScript, React compiler will automatically refresh the web application. 

### Create a Production Build

To create production build of this project, first make sure configuration of dotenv files (`.env` and `.env.production`) are correct, because these configuration **cannot be changed** once the project has been built.

Then run this on console.

```bash
npm run build
```

The production build will on the `build` folder.

For more documentation about deployment, please visit [React deployment documentation page](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md#deployment). 

## Documentation

As this application is build upon Create-React-App, please refer to [Create-React-App Documentation](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

For React Documentation and Tutorial, please refer to [React Documentation](https://reactjs.org/docs) and [React Tutorial](https://reactjs.org/tutorial/tutorial.html).

## Known Issues

1. The implementation of **Oozie Job Details content** is not finished yet.
2. **Yarn Application content** have not been implemented.
3. The service of **job submission to Oozie** have **NOT** been tested since the Oozie server back-end is not with [me](https://github.com/danang-id).

## Built With

Written in [TypeScript](https://github.com/Microsoft/TypeScript), compiled to [ECMAScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript), powered by [React](https://reactjs.org/). 

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/danang-id/baciro-gateway/tags). 

## Authors

* **Danang Galuh Tegar Prasetyo** - [danang-id](https://github.com/danang-id) for PT. DUA EMPAT TUJUH

See also the list of [contributors](https://github.com/danang-id/baciro-gateway/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
