import React from 'react';
import Raven from 'raven-js';

import AuthenticationContainer from './components/authentication/AuthenticationContainer';
import BusinessContainer from './components/business/BusinessContainer';

import ApplicationBase from './services/ApplicationBase';
import UserManagementService from './services/UserManagementService';

export default class System extends React.Component {

  private base: any;
  private props: any;
	private state: any;

  constructor(props) {
    super(props);
    this.base = new ApplicationBase();
    this.handleCredentialsChange = this.handleCredentialsChange.bind(this)
    this.state = {
      credentials: (this.base.cookies.has('credentials') ? this.base.cookies.get('credentials') : null),
    };
    Raven
    .config(process.env.REACT_APP_SENTRY_DSN)
    .install()
  }

  componentsDidCatch(error, errorInfo) {
    if (null !== this.state.credentials) {
      Raven.context( () => {
        Raven.setContext({ credentials: this.state.credentials });
        Raven.captureException(error, { extra: errorInfo });
      });
    } else {
      Raven.captureException(error, { extra: errorInfo });
    }
  }

  handleCredentialsChange(credentialsObject) {
    this.setState({ credentials: credentialsObject });
    if (null !== credentialsObject) this.base.cookies.put('credentials', credentialsObject);
    else this.base.cookies.regenerate();
  }

  render() {
    if (null !== this.state.credentials) {
      return (
        <div id="system-container">
          <link rel="stylesheet" type="text/css" href={ process.env.PUBLIC_URL + "/static/css/matrix-style.css" } />
          <BusinessContainer 
            credentials = { this.state.credentials }
            onCredentialsChange = { this.handleCredentialsChange } >
          </BusinessContainer>
        </div>
      );
    } else {
      return (
        <div id="system-container">
          <link rel="stylesheet" type="text/css" href={ process.env.PUBLIC_URL + "/static/css/matrix-authentication.css" } />
          <AuthenticationContainer
            credentials = { this.state.credentials }
            onCredentialsChange = { this.handleCredentialsChange } >
          </AuthenticationContainer>
        </div>
      );
    } 
  }

}