import RESTService from './RESTService';

export default class GenerateJarService extends RESTService {

	/**
	 * Node Address
	 */
	private static get node() {
		return process.env.REACT_APP_REST_GENERATE_JAR;
	}

	/**
	 * GET methods
	 */
	public static generateJar(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `generate_jar/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(GenerateJarService.node, callback, "GET", endpoint);
	}

	public static downloadJar(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `download_jar/${ "undefined" === typeof parameters[0] || null === parameters[0] ? "" : parameters[0] + "/" }project/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(GenerateJarService.node, callback, "GET", endpoint);
	}

	public static getJar(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `view_jar/${ "undefined" === typeof parameters[0] || null === parameters[0] ? "" : parameters[0] + "/" }project/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(GenerateJarService.node, callback, "GET", endpoint);
	}

}