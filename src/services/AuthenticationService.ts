import RESTService from './RESTService';

export default class AuthenticationService extends RESTService {

	private static get node() {
		return process.env.REACT_APP_REST_USER_MANAGEMENT;
	}

	public static signIn(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `login`; //endpoint `signin` would have been better
		super.executeAnonymous(AuthenticationService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static signUp(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `user`; //endpoint `signup` would have been better
		super.executeAnonymous(AuthenticationService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

}
