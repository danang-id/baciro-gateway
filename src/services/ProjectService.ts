import RESTService from './RESTService';

export default class ProjectService extends RESTService {

	/**
	 * Node Address
	 */
	private static get node() {
		return process.env.REACT_APP_REST_PROJECT;
	}

	/**
	 * GET methods
	 */
	public static getProjectByUser(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] || null === parameters[0] ? "" : parameters[0] + "/" }user/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "GET", endpoint);
	}

	public static getProjectByGroup(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] || null === parameters[0] ? "" : parameters[0] + "/" }group/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "GET", endpoint);
	}

	public static getProjectInventory(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] || null === parameters[0] ? "" : parameters[0] + "/" }inventory/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "GET", endpoint);
	}

	public static getProjectInventoryByUser(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project_inventory_by_user/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(ProjectService.node, callback, "GET", endpoint);
	}

	public static downloadInventory(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `download_inventory/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/project/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "GET", endpoint);
	}

	/**
	 * POST methods
	 */
	public static postProject(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/user/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(ProjectService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static postProjectInventory(callback: Function, formData: object = {}, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/inventory`;
		super.execute(ProjectService.node, callback, "POST", endpoint, formData);
	}

	/**
	 * PUT methods
	 */
	public static putProject(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/user/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static shareProject(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/share/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	/**
	 * DELETE methods
	 */
	public static deleteProject(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(ProjectService.node, callback, "DELETE", endpoint);
	}

	public static deletProjectInventory(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/inventory/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ProjectService.node, callback, "DELETE", endpoint);
	}

}