import ApplicationBase from './ApplicationBase';
import DataFetcher from './DataFetcher';
import FormData from 'formdata-polyfill';

export default class RESTService {

	private static get credentials() {
		return "undefined" !== typeof new ApplicationBase().cookies.cookies.credentials ? new ApplicationBase().cookies.cookies.credentials : {};
	}

	private static createRequest(anonymous: boolean, node: string, endpoint: string = "", options: object = {}) {
		return (anonymous) ? 
			new Request(`${ node }/${ process.env.REACT_APP_REST_MASTER_API_KEY }/${ endpoint }`, options) :
			new Request(`${ node }/${ RESTService.credentials.user_apikey }/${ endpoint }`, options);
	}

	private static doExecute(anonymous: boolean, node: string, callback: Function, method: string, endpoint: string, body: object) {
		const options = (method === "GET" || method === "HEAD" || body === null) ? {
	        method: method, 
	        mode: `cors`, 
	        headers: new Headers({
	        	'Content-Type': 'application/json',
	        }),
	    } : {
	        method: method, 
	        mode: `cors`, 
	        body: `${ JSON.stringify(body) }`,
	        headers: new Headers({
	        	'Content-Type': 'application/json',
	        }),
	    };
	    const request = RESTService.createRequest(anonymous, node, endpoint, options);
		new DataFetcher(request, callback).makeRequest();
	}

	protected static executeAnonymous(node: string, callback: Function, method: string, endpoint: string, body: object = null) {
		RESTService.doExecute(true, node, callback, method, endpoint, body);
	}

	protected static execute(node: string, callback: Function, method: string, endpoint: string, body: object = null) {
		RESTService.doExecute(false, node, callback, method, endpoint, body);
	}

	protected static processFormData(formData: FormData) {
		let body = {};
		for (let pair of formData.entries()) body[pair[0]] = pair[1];
		return body;
	}

}
