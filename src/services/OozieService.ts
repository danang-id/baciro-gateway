import RESTService from './RESTService';

export default class OozieService extends RESTService {

	/**
	 * Node Address
	 */
	private static get node() {
		return process.env.REACT_APP_REST_OOZIE;
	}

	/**
	 * GET methods
	 */
	public static getJobId(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `get_job_id`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobInfo(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_info_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobSuccess(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_success_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobGraph(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_graph_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobDefinition(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_definition_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobLog(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_log_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobAuditLog(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_auditlog_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobStatus(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `job_status_oozie/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getJobHistory(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `get_history_oozie/project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminConfig(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_config_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminStatus(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_status_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminOSEnv(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_osenv_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminJavaSysPro(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_javasyspro_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminIntrumen(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_instrumen_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminVersion(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_version_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminTimezones(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_timezones_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminServers(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_servers_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	public static getAdminShareLib(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `admin_sharelib_oozie`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	/**
	 * POST methods
	 */
	public static submitJob(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `submit_job`;
		super.execute(OozieService.node, callback, "POST", endpoint, super.processFormData(formData));
	}


	public static generateXml(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `generate_xml`;
		super.execute(OozieService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

}