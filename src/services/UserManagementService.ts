import RESTService from './RESTService';

export default class UserManagementService extends RESTService {

	/**
	 * Node Address
	 */
	private static get node() {
		return process.env.REACT_APP_REST_USER_MANAGEMENT;
	}

	/**
	 * GET methods
	 */
	public static getUser(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `user/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "GET", endpoint);
	}

	public static getGroup(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "GET", endpoint);
	}

	public static getGroupByUser(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group/${ "undefined" === typeof parameters[0] || null === parameters[0] ? "" : parameters[0] + "/" }user/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(UserManagementService.node, callback, "GET", endpoint);
	}

	public static getGroupMember(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/member`;
		super.execute(UserManagementService.node, callback, "GET", endpoint);
	}

	public static getUserRole(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `role/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/member`;
		super.execute(UserManagementService.node, callback, "GET", endpoint);
	}

	/**
	 * POST methods
	 */
	public static postUser(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `user`;
		super.execute(UserManagementService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static postGroup(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group`;
		super.execute(UserManagementService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static postGroupMember(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `member`;
		super.execute(UserManagementService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static postUserRole(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `role`;
		super.execute(UserManagementService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static postPrivilege(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `privilege/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	/**
	 * PUT methods
	 */
	public static putUser(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `user/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static putGroup(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static putGroupMember(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `member/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static putUserRole(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `role/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static putPrivilege(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `privilege/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	/**
	 * DELETE methods
	 */
	public static deleteUser(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `user/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "DELETE", endpoint);
	}

	public static deleteGroup(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "DELETE", endpoint);
	}

	public static deleteGroupMember(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `group/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/member/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(UserManagementService.node, callback, "DELETE", endpoint);
	}

	public static deleteUserRole(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `role/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(UserManagementService.node, callback, "DELETE", endpoint);
	}

}