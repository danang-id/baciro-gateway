import ApplicationBase from './ApplicationBase';

export default class DataFetcher {

	private base: ApplicationBase;
	private callback: Function;
	private request: Request;

	constructor(request: Request, callback: Function) {
		this.base = new ApplicationBase();
		this.callback = callback;
		this.request = request;
	}

	private handleResponse = (response: any) => {
		if (!response.ok) throw response;
		return response.text();
	}

	private processResponse = (response: object) => {
		this.callback(true, this.request, response);
	}

	private handleError = (error: any) => {
	    if (typeof error.text === 'function') {
		    error.text().then(text => {
			    this.callback(false, this.request, text);
		  	});
		} else {
			let text = "";
			switch(error) {
				case "TypeError: Failed to fetch":
				default:
					text = "Network error occured. Please verify your network connection. If you have connected to the network but the problem persists, it may be our servers are down at the moment."
			}
		  	this.callback(false, this.request, text);
		}
	}

	public makeRequest(showSplash: boolean = true) {
		if (showSplash) this.base.splash.show();
		if (null === this.callback || null === this.request) {
			throw new Error(`Property callback and request of DataFetcher cannot be null when the object is making request. Try calling setCallback() and setRequest() before makeRequest().`);
		}
		fetch(this.request)
	    .then(this.handleResponse)
	    .then(this.processResponse)
	    .catch(this.handleError)
	    .then(this.base.splash.hide);
	}

}