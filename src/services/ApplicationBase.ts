import React from 'react';
import SecureCookies from 'secure-cookies-js';

import { register, unregister } from './ServiceWorker';

let instance = null;
let running = false;

export default class ApplicationBase {

	public cookies: SecureCookies;
	public elements: any;
	public splash: any;
	public meta: any;
	public serviceWorker: any;
	public _type: any;

	constructor() {
		if (!instance) {
			instance = this;
        }
        this.cookies = new SecureCookies(process.env.REACT_APP_NAME_CODE, process.env.REACT_APP_SECRET);
		this.elements = {
			base: document.getElementsByTagName(process.env.REACT_APP_NAME_SHORT)[0],
			loader: document.getElementsByTagName(`loader`)[0],
		};
		this.splash = {
			show: () => {
				this.elements.loader.style.display = `block`;
        		this.elements.base.style.display = `none`;
			},
			hide: (displayStyle = `block`) => {
				this.elements.loader.style.display = `none`;
		        this.elements.base.style.display = displayStyle;
			},
		};
		this.meta = {
			anagram: {
				url: process.env.PUBLIC_URL + '/anagram',
				data: null,
			},
			name: process.env.REACT_APP_NAME,
			react: {
				version: React.version,
			},
			version: process.env.REACT_APP_VERSION,
		};
		this.serviceWorker = {
			register: register,
			unregister: unregister,
		};
		this._type = 'ApplicationBase';
		return instance;
	}

	run(callback) {
		this.splash.show();
		if (!running) {
			running = true;
			if ('function' === typeof callback) {
				fetch(this.meta.anagram.url + '?t=' + (new Date()).getTime())
				.then((response) => response.text())
				.then((data) => {
					if (data.substr(0, 8) === `#anagram`) {
						this.meta.anagram.data = data.substr(10);
						console.log(this.meta.anagram.data);
					}
					else {
						if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
						    console.log(`Welcome to ${this.meta.name} ${this.meta.version}! No Anagram was detected. Create 'anagram' file and start with #anagram to show yours!`);
						} 
					}
				})
				.catch((error) => {
					if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
					    console.log(`Welcome to ${this.meta.name} ${this.meta.version}! No Anagram was detected. Create 'anagram' file and start with #anagram to show yours!`);
					} 
				})
				.then(() => {
					if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
						console.log(`${this.meta.name} ${this.meta.version} is running in development mode. React version: ${this.meta.react.version}.`);
					}
				})
				.then(() => { callback(); });
			} else {
				throw new Error(`Application Base: run(callback) - callback is not a function.`);
			}
		} else {
			throw new Error(`Application Base: run(callback) - application already running.`);
		}

	}

	sleep(ms) {
	  	return new Promise(resolve => setTimeout(resolve, ms));
	}

}