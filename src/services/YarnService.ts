import RESTService from './RESTService';

export default class YarnService extends RESTService {

	/**
	 * Node Address
	 */
	private static get node() {
		return process.env.REACT_APP_REST_OOZIE;
	}

	/**
	 * GET methods
	 */
	public static getProjectByUser(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/user/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(OozieService.node, callback, "GET", endpoint);
	}

	/**
	 * POST methods
	 */
	public static postProject(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/user/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	/**
	 * PUT methods
	 */
	public static putProject(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/user/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(OozieService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	/**
	 * DELETE methods
	 */
	public static deleteProject(callback: Function, ...parameters: Array<string>) {
		for (let parameter of parameters) if (null === parameter) parameter = "";
		const endpoint = `project/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(OozieService.node, callback, "DELETE", endpoint);
	}

}