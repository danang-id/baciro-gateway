import RESTService from './RESTService';

export default class ClusterService extends RESTService {

	private static get node() {
		return process.env.REACT_APP_REST_CLUSTER;
	}

	public static getCluster(callback: Function, ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(ClusterService.node, callback, "GET", endpoint);
	}

	public static getClusterConfiguration(callback: Function, ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/config/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ClusterService.node, callback, "GET", endpoint);
	}

	public static postCluster(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		const endpoint = `cluster`;
		super.execute(ClusterService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static postClusterConfiguration(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/config`;
		super.execute(ClusterService.node, callback, "POST", endpoint, super.processFormData(formData));
	}

	public static putCluster(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(ClusterService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static putClusterConfiguration(callback: Function, formData: FormData = new FormData(), ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/config/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ClusterService.node, callback, "PUT", endpoint, super.processFormData(formData));
	}

	public static deleteCluster(callback: Function, ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }`;
		super.execute(ClusterService.node, callback, "DELETE", endpoint);
	}

	public static deleteClusterConfiguration(callback: Function, ...parameters: Array<string>) {
		const endpoint = `cluster/${ "undefined" === typeof parameters[0] ? "" : parameters[0] }/config/${ "undefined" === typeof parameters[1] ? "" : parameters[1] }`;
		super.execute(ClusterService.node, callback, "DELETE", endpoint);
	}

}