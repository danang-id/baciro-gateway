import React from 'react';
import ReactDOM from 'react-dom';
import ApplicationBase from './services/ApplicationBase';
import System from './System';

const base = new ApplicationBase();
const __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};

try {
    base.run(main);
}
catch (exception) {
    console.error(exception);
}

function main() {
	return __awaiter(this, void 0, void 0, function* () {
		ReactDOM.render(<System base = { base } />, base.elements.base);
		base.serviceWorker.register();
		yield base.sleep(1000);
		base.splash.hide();
    });
}