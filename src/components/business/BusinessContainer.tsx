import React from 'react';
import { Alert, Button, Modal } from 'react-bootstrap';

import Footer from './Footer';
import ContentContainer from './ContentContainer';
import NavigationBar from './NavigationBar';

import ApplicationBase from './../../services/ApplicationBase';

export default class BusinessContainer extends React.Component {

	private base: any;
	private props: any;
	private state: any;
	private setState: Function;
	private defaultBreadcrumbs: Array<object>;

	constructor(props) {
		super(props);
		this.base = new ApplicationBase();
		this.state = {
			breadcrumbs: [ { icon: "home", title: "Dashboard", page: "dashboard" } ],
			page: 'dashboard',
			modal: {
			 	signout: { show: false, data: {} },
			},
		};
	}

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "signout":
				this.setState({
					modal: {
					 	signout: { show: isShown, data: data },
					}
				});
				break;
			default: break;
		}
	}

	componentWillMount() {
		this.base.splash.show();
	}

	componentDidMount() {
		this.base.splash.hide();
	}

	handlePageChange(page: string) {
		let breadcrumbs = [];
		let changeBreadcrumbs = true;
		if (Array.isArray(this.state.breadcrumbs)) this.state.breadcrumbs.map((value, index, array) => {
			if (changeBreadcrumbs) {
				breadcrumbs.push(value);
				if (value.page === page) changeBreadcrumbs = false;
			}
			return value;
		});
		if (changeBreadcrumbs) {
			breadcrumbs = [ { icon: "home", title: "Dashboard", page: "dashboard" } ];
			switch (page) {
				case "cluster":
					breadcrumbs.push({icon: null, title: "Cluster", page: page});
					break;
				case "cluster-configuration":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Configuration", page: page});
					break;
				case "user":
					breadcrumbs.push({icon: null, title: "User Management", page: page});
					break;
				case "group":
					if (this.props.credentials.user_role_id === 1)
						breadcrumbs.push({icon: null, title: "Group Management", page: page});
					else 
						breadcrumbs.push({icon: null, title: "All Group", page: page});
					break;
				case "group-project":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Project", page: page});
					break;
				case "group-member":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Member", page: page});
					break;
				case "group-my":
					breadcrumbs.push({icon: null, title: "My Group", page: page});
					break;
				case "project":
					breadcrumbs.push({icon: null, title: "My Project", page: page});
					break;
				case "project-inventory":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Inventory", page: page});
					break;
				case "project-generatedjar":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Generated JAR", page: page});
					break;
				case "oozie":
					breadcrumbs.push({icon: null, title: "My Oozie Job", page: page});
					break;
				case "oozie-project":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Oozie Job History", page: page});
					break;
				case "oozie-details":
					breadcrumbs = this.state.breadcrumbs;
					breadcrumbs.push({icon: null, title: "Details", page: page});
					break;
				case "setting":
					breadcrumbs.push({icon: null, title: "Setting", page: page});
					break;
				default: break;
			}
		} else {
			if (null === breadcrumbs || breadcrumbs.length === 0) breadcrumbs = this.state.breadcrumbs;
		}
		this.setState({ page: page, breadcrumbs: breadcrumbs });
	}

	handleSignOut = () => this.modalHandler("signout", true);

	dispatchAction(action: any) {
		switch(action.type) {
			case "SIGN_OUT":
				this.modalHandler("signout");
				if (action.confirmed) {
					this.props.onCredentialsChange(null);
					alert(`Thank you for using ${process.env.REACT_APP_NAME}!`);
				}
				break;
			default: break;
		}
	}

	render() {
		return (
			<div id="business-container">
				<NavigationBar
					credentials = { this.props.credentials }
					onPageChange = { this.handlePageChange.bind(this) }
					onSignOut = { this.handleSignOut.bind(this) }
				/>
				<ContentContainer 
					breadcrumbs = { this.state.breadcrumbs }
					credentials = { this.props.credentials }
					page = { this.state.page }
					onCredentialsChange = { this.props.onCredentialsChange }
					onPageChange = { this.handlePageChange.bind(this) }
				/>
				<Footer 
					appName = { process.env.REACT_APP_NAME }
					providerName = "SOLUSI247"
					providerUrl = "http://www.solusi247.com"
				/> 
				<Modal show = { this.state.modal.signout.show } onHide = { () => { this.modalHandler("signout") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Sign Out</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>
							Hi, { this.props.credentials.user_firstname }!<br></br>
							We're glad to meet you, but we're sad to see you go...<br></br>
							<br></br>
							<strong>Do you really would like to sign out?</strong>
						</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "SIGN_OUT", confirmed: true}) } >
							Yes, Sign Out
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "SIGN_OUT", confirmed: false}) } >
							No, I'll Stay
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}