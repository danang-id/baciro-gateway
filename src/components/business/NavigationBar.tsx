import React from 'react';
import { Image, Nav, Navbar, NavDropdown, NavItem } from 'react-bootstrap';

const NavigationBar = (props) => {
	let menuItem = [];
	if (props.credentials.user_role_id === 1) {
		menuItem.push(
			<NavDropdown key={0} id="group-dropdown" title="Configuration">
				<NavItem eventKey="cluster">Cluster</NavItem>
		    </NavDropdown>
		);
		menuItem.push(<NavItem key={1} eventKey="user">Users</NavItem>);
		menuItem.push(<NavItem key={2} eventKey="group">Groups</NavItem>);
		menuItem.push(<NavItem key={3} eventKey="oozie">My Oozie Job</NavItem>);
	} else if (props.credentials.user_role_id === 2) {
		menuItem.push(
			<NavDropdown key={0} id="group-dropdown" title="Group">
				<NavItem eventKey="group-my">My Group</NavItem>
		        <NavItem eventKey="group">All Groups</NavItem>
		    </NavDropdown>
		);
		menuItem.push(<NavItem key={1} eventKey="project">My Project</NavItem>);
		menuItem.push(<NavItem key={2} eventKey="oozie">My Oozie Job</NavItem>);
	}
	return (
		<div id="user-nav" className="navbar navbar-inverse navbar-fixed-top">
			<Nav id="left-nav" pullLeft onSelect={ props.onPageChange }>
				<NavItem eventKey="dashboard">
					<Image
						style={{ height: "22px" }}
						src={`${process.env.PUBLIC_URL}/static/img/dashboard.png`}
					/>
				</NavItem>
        		<NavItem eventKey="dashboard">Dashboard</NavItem>
        		{ menuItem.map(element => element) }
		    </Nav>
		    <Nav id="right-nav" pullRight onSelect={ props.onPageChange }>
		       	<NavDropdown id="profile-dropdown" title={ props.credentials.user_email }>
		        	<NavItem eventKey="setting">Setting</NavItem>
		        	<NavItem onClick={ props.onSignOut }>Sign Out</NavItem>
		        </NavDropdown>
			</Nav>
		</div>
	);
}

export default NavigationBar;