import React from 'react';
import ReactDOM from 'react-dom';
import { Alert, Button } from 'react-bootstrap';

import DashboardContent from './contents/dashboard/DashboardContent';
import ClusterContent from './contents/configuration/ClusterContent';
import ClusterConfigurationContent from './contents/configuration/ClusterConfigurationContent';
import UserContent from './contents/user/UserContent';
import GroupContent from './contents/group/GroupContent';
import GroupMemberContent from './contents/group/GroupMemberContent';
import MyGroupContent from './contents/group/MyGroupContent';
import ProjectContent from './contents/project/ProjectContent';
import ProjectInventoryContent from './contents/project/ProjectInventoryContent';
import ProjectGeneratedJARContent from './contents/project/ProjectGeneratedJARContent';
import OozieContent from './contents/oozie/OozieContent';
import OozieDetailsContent from './contents/oozie/OozieDetailsContent';
import SettingContent from './contents/setting/SettingContent';

export default class ContentContainer extends React.Component {

	private props: any;
	private state: any;
	private setState: Function;

	constructor(props) {
		super(props);
		this.state = {
			alert: null,
			data: null,
		}
	}

	componentWillReceiveProps = (nextProps) => (this.props.page !== nextProps.page) ? this.setState({ alert: null }) : null;

	handleAlertChange(alert: object) {
		this.setState({ alert: alert });
	}

	handlePageChange(page: string, data: object = null) {
		this.setState({ data: data });
		this.handleAlertChange(null);
		this.props.onPageChange(page);
	}

	render() {
		const alert = (null !== this.state.alert) ? (
        	<Alert bsStyle={ this.state.alert.style } onDismiss={ () => { this.handleAlertChange(null) } } className="fade-in" closeLabel="" >
          		<strong>{ this.state.alert.title }</strong>
          		<p>{ this.state.alert.message }</p>
        	</Alert>
		) : null;
		const breadcrumbs = this.props.breadcrumbs;
		let mainContent = null;
		const page = this.props.page;
		switch (page) {
			case 'cluster':
				mainContent = (
					<ClusterContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'cluster-configuration':
				mainContent = (
					<ClusterConfigurationContent
						cluster = { this.state.data.cluster }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'user': 
				mainContent = (
					<UserContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'group':  
				mainContent = (
					<GroupContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'group-project':  
				mainContent = (
					<ProjectContent
						group = { this.state.data.group }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'group-member':  
				mainContent = (
					<GroupMemberContent
						group = { this.state.data.group }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'group-my': 
				mainContent = (
					<MyGroupContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'project': 
				mainContent = (
					<ProjectContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'project-inventory': 
				mainContent = (
					<ProjectInventoryContent
						project = { this.state.data.project }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'project-generatedjar': 
				mainContent = (
					<ProjectGeneratedJARContent
						project = { this.state.data.project }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'oozie':  
				mainContent = (
					<OozieContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'oozie-project':  
				mainContent = (
					<OozieContent
						project = { this.state.data.project }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'oozie-details':  
				mainContent = (
					<OozieDetailsContent
						oozie = { this.state.data.oozie }
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'setting':  
				mainContent = (
					<SettingContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onCredentialsChange = { this.props.onCredentialsChange }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
			case 'dashboard': 
			default:
				mainContent = (
					<DashboardContent
						credentials = { this.props.credentials }
						showAlert = { this.handleAlertChange.bind(this) }
						onPageChange = { this.handlePageChange.bind(this) }
					/>
				);
				break;
		}
		return (
			<div id="content">
			    <div className="container-fluid">
			      	<div id="breadcrumb">
			      		<div className="row-fluid">
			      			{ breadcrumbs.map((breadcrumb, key) => (
			      				<BreadcrumbItem
			      					key={key}
									icon={breadcrumb.icon}
									info={breadcrumb.info}
								   	title={breadcrumb.title}
								   	onClick={()=>{this.props.onPageChange(breadcrumb.page)}}
								/>
			      			)) }
			      		</div>
			      	</div>
			      	<div id="alert" className="row-fluid" style={ (null !== alert) ? { display: "inline-block" } : { display: "none" } }>
			      		<br></br>
			      		{ alert }
			      	</div>
			      	<div id="main-content">
						{ mainContent  }
			      	</div>
			      	<div className="row-fluid">
			      		<br></br><br></br><br></br><br></br>
			      	</div>
			    </div>
			</div>
		);
	}

}

const BreadcrumbItem = (props) => (
	<a href="#" title={ props.info } onClick={ props.onClick } className="tip-bottom">
		{ (null !== props.icon) ? (<i className={`icon-${props.icon}`}></i>) : '' }
		{ props.title }
	</a>
);