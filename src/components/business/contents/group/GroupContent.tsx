import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import UserManagementService from './../../../../services/UserManagementService';

export default class GroupContent extends React.Component {
	
	private state: any;
	private props: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	edit: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			 	groupmember: { show: false, data: {} },
			},
			pageOfData: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_GROUP" });

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			groupmember: { show: false, data: {} },
					}
				});
				break;
			case "edit":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
			 			groupmember: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
			 			groupmember: { show: false, data: {} },
					}
				});
				break;
			case "groupmember":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			groupmember: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_GROUP": 
				UserManagementService.getGroup((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Group!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	});
		      	break;
			case "VIEW_PROJECT":
				this.props.onPageChange("group-project", { group: action.data });
				break;
			case "VIEW_GROUPMEMBER":
				this.props.onPageChange("group-member", { group: action.data });
				break;
			case "CREATE_GROUP":
				this.modalHandler("create", true);
				break;
			case "CREATE_GROUPMEMBER":
				UserManagementService.getGroupMember((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	let unlocked = false;
				            	for (let datum of data)
				            		if (datum.user_id === this.props.credentials.user_id && datum.member_role === "Administrator") 
				            			unlocked = true;
				            	if (unlocked)
									UserManagementService.getUser(
									(success: boolean, request: Request, response: any) => {
							      		if (success) {
							        		try {
							        			const feedback = JSON.parse(response);
									            if (feedback.err_code === 0) {
									            	const group = action.data;
									            	const users = feedback.data;
									            	const data = { group_id: group.group_id, group_name: group.group_name, users: users }
									            	this.modalHandler("groupmember", true, data);
									            } else {
									            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
									            	this.props.showAlert({ 
									            		style: `danger`, 
									            		title: `Failed to Get List of Users for Adding New Member!`, 
									            		message: `${error}`
									            	});
									            }
									        } catch (exception) {
									        	this.props.showAlert({ 
									        		style: `danger`, 
									        		title: `Oops! Something is not right...`,
									        		message: `${response}`
									        	});
									        }
									    } else {
									    	this.props.showAlert({
									    		style: `danger`, 
									    		title: `Oops! Something is not right...`, 
									    		message: `${response}`
									    	});
									    }
							      	});
				            	else this.props.showAlert({ style: `danger`, title: `Failed to to Add New Group Member: Limited Permission for Non-Administrative Member!`, message: `You are only able to see the member of group ${ action.data.group_name }. Only group administrative members that are able to modify group's members.`});
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Check Group Permission!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, action.data.group_id);
				break;
			case "EDIT_GROUP":
				this.modalHandler("edit", true, action.data);
				break;
			case "DELETE_GROUP":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_GROUP":
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-group-create");
					const formData = new FormData(formElement);
					UserManagementService.postGroup((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Stored Successfully!`,
					            		message: `Group ${ feedback.data[0].group_name } has been successfully stored.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Store Group!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUP" });
			      	}, formData, formData.get("group_id"));
				} 
				break;
				case "STORE_GROUPMEMBER":
				this.modalHandler("groupmember");
				if (action.confirmed) {
					const formElement = document.getElementById("form-groupmember-create");
					const formData = new FormData(formElement);
					UserManagementService.postGroupMember((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Member Added Successfully!`,
					            		message: `New member ${ feedback.data.user_firstname } ${ feedback.data.user_lastname } has been successfully added to group ${ feedback.data.group_name }.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Add New Group Member!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUP" });
			      	}, formData);
				} 
				break;
			case "UPDATE_GROUP":
				this.modalHandler("edit");
				if (action.confirmed) {
					const formElement = document.getElementById("form-group-edit");
					const formData = new FormData(formElement);
					if (this.state.modal.edit.data.group_name === formData.get("group_name")) formData.delete("group_name");
					if (this.state.modal.edit.data.group_status === formData.get("group_status")) formData.delete("group_status");
					UserManagementService.putGroup((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Updated Successfully!`,
					            		message: `Group ${ feedback.data[0].group_name } has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Group!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUP" });
			      	}, formData, formData.get("group_id"));
				} 
				break;
			case "DESTROY_GROUP":
				const groupId = this.state.modal.delete.data.group_id;
				this.modalHandler("delete");
				if (action.confirmed) {
					UserManagementService.deleteGroup((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Deleted Successfully!`,
					            		message: `Group has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete Group!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUP" });
			      	}, groupId);
				} 
				break;
			default:
				return;
		}
	}

	statusFormatter = (cell, row) => {
		switch(cell) {
			case "true":
				return (<font color="green">Active</font>);
			case "false":
				return (<font color="red">Not Active</font>);
			default:
				return row.group_status; 
		}
	};

	projectFormatter = (cell, row) => (
		<div>
			{ (this.props.credentials.user_role_id === 1) ? (
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_PROJECT", data: row }) } >
				View Project
			</Button>
			) : "" }
		</div>
	);

	memberFormatter = (cell, row) => (
		<div>
			{ (this.props.credentials.user_role_id === 1) ? (
			<Button
				bsStyle="success" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "CREATE_GROUPMEMBER", data: row }) } >
				Add
			</Button>
			) : "" }
			&nbsp;
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_GROUPMEMBER", data: row }) } >
				View
			</Button>
		</div>
	);

	actionsFormatter = (cell, row) => (this.props.credentials.user_role_id === 1) ? (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_GROUP", data: row }) } >
				Edit
			</Button>
			&nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_GROUP", data: row }) } >
				Delete
			</Button>
		</div>
	) : "";

	renderUserList = () => {
		const users = ("undefined" !== typeof this.state.modal.groupmember.data.users) ? this.state.modal.groupmember.data.users : [];
		const options = [];
		users.map((value, index, array) => {
			return options.push(
				<option key={index} value={ value.user_id }>
					{ value.user_firstname } { value.user_lastname } [{ value.user_email }]
				</option>);
		});
		return options;
	}

	render() {
		const addGroupButton = (this.props.credentials.user_role_id === 1) ? (
				<div className="row-fluid">
					<div className="span6" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_GROUP" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_GROUP" }) }>
							Add Group <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
		) : (
				<div className="row-fluid">
					<div className="span12" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_GROUP" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
				</div>
		);
		const tableOptions = {
			noDataText: (<center>It seems that there is no any group at the moment.</center>),
		};
		const table = (this.props.credentials.user_role_id === 1) ? (
								<BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn
							        	dataField = "group_name" isKey
							        	headerAlign = "center" dataSort>
							        	Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_status"
							        	headerAlign = "center" dataAlign = "center" dataSort dataFormat = { this.statusFormatter } >
							        	Status
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_update_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Last Updated At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_project"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.projectFormatter } >
							        	Project
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_member"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.memberFormatter } >
							        	Members
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
		) : (
								<BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn
							        	dataField = "group_name" isKey
							        	headerAlign = "center" dataSort>
							        	Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_status"
							        	headerAlign = "center" dataAlign = "center" dataSort dataFormat = { this.statusFormatter } >
							        	Status
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_update_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Last Updated At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_member"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.memberFormatter } >
							        	Members
							        </TableHeaderColumn>
							    </BootstrapTable>
		);
		return (
			<div id="group-content">
				{ addGroupButton }
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>Group List</h5>
					      	</div>
							<div>
							    { table }
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create Group</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-group-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_GROUP", confirmed: true})
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="group_name"
										placeholder="Group Name" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Status:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="group_status"
										defaultValue={true}
										required>
										<ToggleButton value={true}><font color="green">Active</font></ToggleButton>
										<ToggleButton value={false}><font color="red">Not Active</font></ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_GROUP", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_GROUP", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Edit Group</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-group-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_GROUP", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="group_id" 
								defaultValue={ this.state.modal.edit.data.group_id } />
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="group_name"
										placeholder="Group Name" 
										defaultValue={ this.state.modal.edit.data.group_name }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Status:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="group_status"
										defaultValue={ this.state.modal.edit.data.group_status }
										required>
										<ToggleButton value={"true"}><font color="green">Active</font></ToggleButton>
										<ToggleButton value={"false"}><font color="red">Not Active</font></ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_GROUP", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_GROUP", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Group</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete group <strong>{ this.state.modal.delete.data.group_name }</strong>?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_GROUP", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_GROUP", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.groupmember.show } onHide = { () => { this.modalHandler("groupmember") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Add Member to Group { this.state.modal.groupmember.data.group_name }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-groupmember-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_GROUPMEMBER", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="group_id" 
								defaultValue={ this.state.modal.groupmember.data.group_id } />
							<div className="control-group">
								<label className="control-label">Choose a User:</label>
								<div className="controls">
									<FormControl 
										componentClass="select"
										name="user_id" >
										{ this.renderUserList() }
									</FormControl>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_GROUPMEMBER", confirmed: true}) } >
							Add This Member to { this.state.modal.groupmember.data.group_name  } 
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_GROUPMEMBER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}