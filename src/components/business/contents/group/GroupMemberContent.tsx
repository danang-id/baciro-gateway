import React from 'react';
import { Alert, Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import UserManagementService from './../../../../services/UserManagementService';

export default class GroupMemberContent extends React.Component {
	
	private state: any;
	private props: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	edit: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			},
			pageOfData: [],
			unlocked: false,
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_GROUPMEMBER" });

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			groupmember: { show: false, data: {} },
					}
				});
				break;
			case "edit":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
			 			groupmember: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
			 			groupmember: { show: false, data: {} },
					}
				});
				break;
			case "groupmember":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			groupmember: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_GROUPMEMBER": 
				UserManagementService.getGroupMember((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	for (let datum of data)
				            		if (datum.user_id === this.props.credentials.user_id && datum.member_role === "Administrator")
				            			this.setState({ unlocked: true });
				            	this.setState({ data: data })
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Member of Group ${ this.props.group.group_name }!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, this.props.group.group_id);
		      	break;
			case "CREATE_GROUPMEMBER":
				UserManagementService.getUser((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const users = feedback.data;
				            	const data = { group_id: this.props.group.group_id, group_name: this.props.group.group_name, users: users }
				            	this.modalHandler("create", true, data);
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Get List of User for Adding Member!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	});
				break;
			case "EDIT_GROUPMEMBER":
				this.modalHandler("edit", true, action.data);
				break;
			case "DELETE_GROUPMEMBER":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_GROUPMEMBER":
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-groupmember-create");
					const formData = new FormData(formElement);
					UserManagementService.postGroupMember((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Member Added Successfully!`,
					            		message: `New member ${ feedback.data.user_firstname } ${ feedback.data.user_lastname } has been successfully added to group ${ feedback.data.group_name }.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Add New Group Member!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUPMEMBER" });
			      	}, formData);
				} 
				break;
			case "UPDATE_GROUPMEMBER":
				this.modalHandler("edit");
				if (action.confirmed) {
					const formElement = document.getElementById("form-groupmember-edit");
					const formData = new FormData(formElement);
					UserManagementService.putGroupMember((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Member Updated Successfully!`,
					            		message: `Group Member ${ feedback.data.user_firstname } ${ feedback.data.user_lastname } has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Group Member!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUPMEMBER" });
			      	}, formData, formData.get("member_id"));
				} 
				break;
			case "DESTROY_GROUPMEMBER":
				const groupId = this.props.group.group_id;
				const memberId = this.state.modal.delete.data.member_id;
				this.modalHandler("delete");
				if (action.confirmed) {
					UserManagementService.deleteGroupMember((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Group Member Deleted Successfully!`,
					            		message: `Group Member has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete Group Member!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_GROUPMEMBER" });
			      	}, groupId, memberId);
				} 
				break;
			default:
				return;
		}
	}

	nameFormatter = (cell, row) => `${ row.user_firstname } ${ row.user_lastname }`;

	statusFormatter = (cell, row) => {
		switch(cell) {
			case "Active":
				return (<font color="green">Active</font>);
			case "Inactive":
				return (<font color="red">Not Active</font>);
			default:
				return row.group_status; 
		}
	};

	actionsFormatter = (cell, row) => (
		(row.member_role !== "Administrator") ?
			(this.state.unlocked) ? (
				<div>
					<Button
						bsStyle="danger" className="btn-mini"
						onClick={ () => this.dispatchAction({ type: "DELETE_GROUPMEMBER", data: row }) } >
						Delete
					</Button>
				</div>
			) : "No action available."
		: "Group Administrator"
	);

	renderUserList = () => {
		const users = ("undefined" !== typeof this.state.modal.create.data.users) ? this.state.modal.create.data.users : [];
		const options = [];
		users.map((value, index, array) => {
			return options.push(
				<option key={value.user_id.toString()} value={ value.user_id }>
					{ value.user_firstname } { value.user_lastname } [{ value.user_email }]
				</option>);
		});
		return options;
	}

	render() {
		const addGroupMemberButton = (this.state.unlocked) ? (
				<div className="row-fluid">
					<div className="span6" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_GROUPMEMBER" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_GROUPMEMBER" }) }>
							Add Member to the Group <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
		) : (
			<Alert bsStyle="danger">
          		<strong>Limited Permission for Non-Administrative Member</strong>
          		<p>You are only able to see the member of group { this.props.group.group_name }. Only group administrative members that are able to modify group's members.</p>
        	</Alert>
		);

		const tableOptions = {
			noDataText: (<center>It seems that this group ({ this.props.group.group_name }) has no any member the moment.</center>),
		};
		return (
			<div id="group-member-content">
				{ addGroupMemberButton }
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>Member of Group { this.props.group.group_name }</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn
							        	dataField = "member_name" isKey
							        	headerAlign = "center" dataSort dataFormat={ this.nameFormatter }>
							        	Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "user_email"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	E-mail Address
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "member_status"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.statusFormatter } >
							        	Status
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "group_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Add Member to Group { this.state.modal.create.data.group_name }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-groupmember-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_GROUPMEMBER", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="group_id" 
								defaultValue={ this.state.modal.create.data.group_id } />
							<div className="control-group">
								<label className="control-label">Choose a User:</label>
								<div className="controls">
									<FormControl 
										componentClass="select"
										name="user_id" >
										{ this.renderUserList() }
									</FormControl>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_GROUPMEMBER", confirmed: true}) } >
							Add This Member to { this.state.modal.create.data.group_name  } 
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_GROUPMEMBER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Update Member Status</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-groupmember-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_GROUPMEMBER", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="group_id" 
								defaultValue={ this.props.group.group_id } />
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.modal.edit.data.user_id } />
							<FormControl 
								type="hidden"
								name="member_id" 
								defaultValue={ this.state.modal.edit.data.member_id } />
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										placeholder="Name" 
										value={ `${this.state.modal.edit.data.user_firstname} ${this.state.modal.edit.data.user_lastname}` }
										readOnly />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Status:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="member_status"
										defaultValue={ this.state.modal.edit.data.member_status }
										required>
										<ToggleButton value={"true"}><font color="green">Active</font></ToggleButton>
										<ToggleButton value={"false"}><font color="red">Not Active</font></ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_GROUPMEMBER", confirmed: true}) } >
							Update Status
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_GROUPMEMBER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Group Member</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete member <strong>{ this.state.modal.delete.data.user_firstname } { this.state.modal.delete.data.user_lastname }</strong> of group { this.props.group.group_name }?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_GROUPMEMBER", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_GROUPMEMBER", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}