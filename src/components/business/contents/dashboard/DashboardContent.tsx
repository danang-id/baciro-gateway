import React from 'react';
import { BarChart, PieChart } from 'rd3';
import { Button } from 'react-bootstrap';

import OozieService from './../../../../services/OozieService';
import ApplicationBase from './../../../../services/ApplicationBase';

export default class OozieContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			statusFetched: 0,
			statusFailingTime: 0,
			mounted: false,
			realtime: false,
			data: [],
			chartData: [],
			dailyRateChart: `Getting data...`,
			dailySubmissionChart: `Getting data...`,
			weeklyRateChart: `Getting data...`,
			weeklySubmissionChart: `Getting data...`,
			allTimeRateChart: `Getting data...`,
		};
	}

	componentDidMount = () => {
		if ("undefined" !== typeof new ApplicationBase().cookies.cookies.credentials) this.dispatchAction({ type: "LOAD_OOZIE" });
		this.setState({ mounted: true });
	}
	componentDidUpdate = (prevProps, prevState) => {
		if ("undefined" === typeof new ApplicationBase().cookies.cookies.credentials) {
			this.dispatchAction({ type: "LOAD_OOZIE" });
		}
	}
	componentWillUnmount = () => {
		this.setState({ mounted: false });
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_OOZIE": 
				OozieService.getJobId((success: boolean, request: Request, response: any) => {
					if (!this.state.mounted) return;
					let message = null;
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	if (this.state.realtime && Array.isArray(feedback.data)) data.map((value, index, array) => {
				            		this.dispatchAction({ type: "LOAD_STATUS", data: value, status: value.oozie_status });
									value.oozie_realtime = true;
				            		return value;
				            	});
				            	this.setState({ data: data });
				            } else {
				            	message = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 2: message = `You don't have any oozie job recorded at the moment.`; break;
				            		default: break;
				            	}
				            }
				        } catch (exception) {
				        	message = `Data is not available at the moment.`;
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
						message = `Data is not available at the moment.`;
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
				    if (this.state.realtime) {
				    	if (null === message) message = `Getting real-time status of all jobs...`;
				    } else {
				    	if (null === message) message = `Computing data...`;
				    }
				    this.setState({ dailyRateChart: message });
					this.setState({ dailySubmissionChart: message });
					this.setState({ weeklyRateChart: message });
					this.setState({ weeklySubmissionChart: message });
					this.setState({ allTimeRateChart: message });
					if (!this.state.realtime) this.dispatchAction({ type: "COMPUTE_DATA" });
		      	});
		      	break;
		    case "LOAD_STATUS":
		    	if (action.data instanceof Object) {
		    		OozieService.getJobStatus((success: boolean, request: Request, response: any) => {
		    			if (!this.state.mounted) return;
		    			this.setState({ statusFetched: ++this.state.statusFetched });
		    			let fails = true;
		      			if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	fails = false;
					            	const data = this.state.data;
					            	if (Array.isArray(data)) data.map((value, index, array) => {
					            		if (value.oozie_job_id === action.data.oozie_job_id) {
					            			value.oozie_status = feedback.data.oozie_status;
					            			value.oozie_realtime = true;
					            		}
					            		return value;
					            	});
					            	this.setState({ data: data });
					            } else {
					            	const data = this.state.data;
					            	if (Array.isArray(data)) data.map((value, index, array) => {
					            		if (value.oozie_job_id === action.data.oozie_job_id) {
					            			value.oozie_status = action.status;
					            			value.oozie_realtime = false;
					            		}
					            		return value;
					            	});
					            	this.setState({ data: data });
					            	this.setState({ statusFailingTime: ++this.state.statusFailingTime });
					            }
					        } catch (exception) {
					        	const data = this.state.data;
					            if (Array.isArray(data)) data.map((value, index, array) => {
					            	if (value.oozie_job_id === action.data.oozie_job_id) {
					            		value.oozie_status = action.status;
					            		value.oozie_realtime = false;
					            	}
					            	return value;
					            });
					            this.setState({ data: data });
					            this.setState({ statusFailingTime: ++this.state.statusFailingTime });
					        }
					    } else {
							const data = this.state.data;
								if (Array.isArray(data)) data.map((value, index, array) => {
									if (value.oozie_job_id === action.data.oozie_job_id) {
										value.oozie_status = action.status;
					            		value.oozie_realtime = false;
									}
								return value;
							});
							this.setState({ data: data });
							this.setState({ statusFailingTime: ++this.state.statusFailingTime });
					    }
					    if (this.state.statusFailingTime >= this.state.data.length) {
					    	this.props.showAlert({ 
					    		style: `danger`, 
					    		title: `Failed to Load Real-Time Status of ALL Oozie Jobs!`, 
					    		message: `We are not capable to fetch the status data of ALL Oozie Jobs at this moment. It looks like Oozie server couldn't be contacted right now.`
					    	});
					    	this.setState({ realtime: false });
					    } else if (fails) {
					    	this.props.showAlert({ 
					    		style: `warning`, 
					    		title: `Warning: Some of oozie jobs' status are NOT realtime!`, 
					    		message: `Number of realtime failed status: ${ this.state.statusFailingTime } of ${ this.state.data.length } oozie job(s).`
					    	});
					    }
					    if (this.state.statusFetched >= this.state.data.length) {
					    	const message = `Computing data...`;
						    this.setState({ dailyRateChart: message });
							this.setState({ dailySubmissionChart: message });
							this.setState({ weeklyRateChart: message });
							this.setState({ weeklySubmissionChart: message });
							this.setState({ allTimeRateChart: message });
							this.dispatchAction({ type: "COMPUTE_DATA" });
					    }
			      	}, action.data.oozie_job_id);
		    	}
		    	break;
			case "COMPUTE_DATA":
				// Set Date and Time Counter
				const now = new Date();
			    const oneDayAgo = new Date(); oneDayAgo.setDate(now.getDate() - 1);
			    const oneWeekAgo = new Date(); oneWeekAgo.setDate(now.getDate() - 7);
			    const jobsToday = [];
			    const jobsThisWeek = [];
			    const data = this.state.data;
			    // Divide all the jobs depends on the date or time
			    // (on Daily basis and Weekly basis)
			    if (Array.isArray(data)) data.map((value, index, array) => {
			    	const date = new Date(value.oozie_create_date);
			        if (now >= date && oneDayAgo <= date) jobsToday.push(value);
			        if (now >= date && oneWeekAgo <= date) jobsThisWeek.push(value);
			        return value;
			    });
			    // Calculate the Daily basis jobs
			    if (jobsToday.length > 0) {
			    	// Calculate Daily Jobs !
			    	// Job Rate Data: Array of Objects with 2 keys: "label" (status) and "value" (number)
			    	const dailyRateData = [];
			    	if (Array.isArray(jobsToday)) jobsToday.forEach((value, index, array) => {
			    		let hasBeenRecorded = false;
			    		dailyRateData.forEach((chartDataValue, chartDataIndex, chartDataArray) => {
			    			if (value.oozie_status === chartDataValue.label) {
			    				hasBeenRecorded = true;
			    				chartDataValue.value++;
			    			} 
			    		});
			    		if (!hasBeenRecorded) {
			    			const chartDataObject = { label: value.oozie_status, value: 1 };
			    			dailyRateData.push(chartDataObject);
			    		}
			    	});
			    	if (Array.isArray(dailyRateData)) dailyRateData.forEach((value, index, array) => {
			    		value.value = (value.value / jobsToday.length) * 100;
			    	});
			    	// Job Submit Data: Array of Objects with 2 keys: "x" (time) and "y" (number)
			    	const dailySubmissionData = []
			    	if (Array.isArray(jobsThisWeek)) jobsThisWeek.forEach((value, index, array) => {
			    		const date = new Date(value.oozie_create_date);
			    		const time = `${ date.getMonth() + 1 }/${ date.getDate() } ${ date.getHours() }:00`;
			    		let hasBeenRecorded = false;
			    		dailySubmissionData.forEach((chartDataValue, chartDataIndex, chartDataArray) => {
			    			if (time === chartDataValue.x) {
			    				hasBeenRecorded = true;
			    				chartDataValue.y++;
			    			} 
			    		});
			    		if (!hasBeenRecorded) {
			    			const chartDataObject = { x: time, y: 1 };
			    			dailySubmissionData.push(chartDataObject);
			    		}
			    	});
			    	// Rendering the Charts
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "dailyRateChart", chartData: dailyRateData });
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "dailySubmissionChart", chartData: dailySubmissionData });
			    } else {
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "dailyRateChart", chartData: [] });
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "dailySubmissionChart", chartData: [] });
			    }
			    // Calculate the Weekly basis jobs
			    if (jobsThisWeek.length > 0) {
			    	// Calculate Weekly Jobs !
			    	// Job Rate Data: Array of Objects with 2 keys: "label" (status) and "value" (number)
			    	const weeklyRateData = [];
			    	if (Array.isArray(jobsThisWeek)) jobsThisWeek.forEach((value, index, array) => {
			    		let hasBeenRecorded = false;
			    		weeklyRateData.forEach((chartDataValue, chartDataIndex, chartDataArray) => {
			    			if (value.oozie_status === chartDataValue.label) {
			    				hasBeenRecorded = true;
			    				chartDataValue.value++;
			    			} 
			    		});
			    		if (!hasBeenRecorded) {
			    			const chartDataObject = { label: value.oozie_status, value: 1 };
			    			weeklyRateData.push(chartDataObject);
			    		}
			    	});
			    	if (Array.isArray(weeklyRateData)) weeklyRateData.forEach((value, index, array) => {
			    		value.value = (value.value / jobsThisWeek.length) * 100;
			    	});
			    	// Job Submit Data: Array of Objects with 2 keys: "x" (dayDate) and "y" (number)
			    	const weeklySubmissionData = []
			    	const days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			    	if (Array.isArray(jobsThisWeek)) jobsThisWeek.forEach((value, index, array) => {
			    		const date = new Date(value.oozie_create_date);
			    		const dayDate = `${ days[date.getDay()] } (${ date.getMonth() + 1 }/${ date.getDate() })`;
			    		let hasBeenRecorded = false;
			    		weeklySubmissionData.forEach((chartDataValue, chartDataIndex, chartDataArray) => {
			    			if (dayDate === chartDataValue.x) {
			    				hasBeenRecorded = true;
			    				chartDataValue.y++;
			    			} 
			    		});
			    		if (!hasBeenRecorded) {
			    			const chartDataObject = { x: dayDate, y: 1 };
			    			weeklySubmissionData.push(chartDataObject);
			    		}
			    	});
			    	// Rendering the Charts
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "weeklyRateChart", chartData: weeklyRateData });
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "weeklySubmissionChart", chartData: weeklySubmissionData });
			    } else {
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "weeklyRateChart", chartData: [] });
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "weeklySubmissionChart", chartData: [] })
			    }
			    // Calculate all the jobs
			    if (data.length > 0) {
			    	// Calculate All-Time Jobs !
			    	// Job Rate Data: Array of Objects with 2 keys: "label" (status) and "value" (number)
			    	const allTimeRateDate = [];
			    	if (Array.isArray(data)) data.forEach((value, index, array) => {
			    		let hasBeenRecorded = false;
			    		allTimeRateDate.forEach((chartDataValue, chartDataIndex, chartDataArray) => {
			    			if (value.oozie_status === chartDataValue.label) {
			    				hasBeenRecorded = true;
			    				chartDataValue.value++;
			    			} 
			    		});
			    		if (!hasBeenRecorded) {
			    			const chartDataObject = { label: value.oozie_status, value: 1 };
			    			allTimeRateDate.push(chartDataObject);
			    		}
			    	});
			    	if (Array.isArray(allTimeRateDate)) allTimeRateDate.forEach((value, index, array) => {
			    		value.value = (value.value / data.length) * 100;
			    	});
			    	// Rendering the Charts
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "allTimeRateChart", chartData: allTimeRateDate });
			    } else {
			    	this.dispatchAction({ type: "DISPLAY_CHART", chartType: "allTimeRateChart", chartData: [] });
			    }
				break;
			case "DISPLAY_CHART":
				let stateObject = {};
				stateObject[action.chartType] = this.renderChart(action.chartType, action.chartData);
				this.setState(stateObject);
				break;
			default: return;
		}
	}

	renderChart(chartType, data: Array<any>) {
		if (chartType === void 0) return;
		switch (chartType) {
			case "dailyRateChart":
				return data.length > 0 ? (
					<PieChart
						data = { data }
						width = { 400 } height = { 175 }
						radius = { 75 } innerRadius = { 15 }
						title="Job Submission Rate (Today)" />
				) : `You don't have any job recorded in the past 24 hours.`;
			case "dailySubmissionChart":
				return data.length > 0 ? (
					<BarChart
						data = { data }
						width = { 400 }
						height = { 175 }
						title="Job Submission (Today)"
						xAxisLabel="Time (Hour)"
						yAxisLabel="Submission" />
				) : `You don't have any job recorded in the past 24 hours.`;
			case "weeklyRateChart":
				return data.length > 0 ? (
					<PieChart
						data = { data }
						width = { 400 } height = { 175 }
						radius = { 75 } innerRadius = { 15 }
						title="Job Submission Rate (This Week)" />
				) : `You don't have any job recorded in the past 7 days.`;
			case "weeklySubmissionChart":
				return data.length > 0 ? (
					<BarChart
						data = { data }
						width = { 400 }
						height = { 175 }
						title="Job Submission (This Week)"
						xAxisLabel="Day and Date"
						yAxisLabel="Submission" />
				) : `You don't have any job recorded in the past 7 days.`;
			case "allTimeRateChart":
				return data.length > 0 ? (
					<PieChart
						data = { data }
						width = { 600 } height = { 200 }
						radius = { 100 } innerRadius = { 20 }
						title="Job Submission Rate (All-Time)" />
				) : `You never have any job recorded all of the time.`;
			default: return;
		}
	}

	render() {
		return (
			<div id="dashboard">
				<div className="row-fluid">
					<div className="span6">
					    <h4>Hi, { `${ this.props.credentials.user_firstname } ${ this.props.credentials.user_lastname }` }!<br></br>
					    <small>Let's get started by wrapping your activies...</small></h4>
					</div>
					<div className="span6">
						<div className="pull-right"  style={{ padding: "5px 0px 10px 0px" }}>
							 { 
								( this.state.realtime ) ? (
									<Button bsStyle="success"
										onClick={ () => {
											this.setState({ realtime: false });
											this.dispatchAction({ type: "LOAD_OOZIE" });
										}}>
										Real-Time Status: ON
									</Button>
								) : (
									<Button bsStyle="danger" 
										onClick={ () => {
											this.setState({ realtime: true });
											this.dispatchAction({ type: "LOAD_OOZIE" });
										}}>
										Real-Time Status: OFF
									</Button>
								) 
							}
							&nbsp;
							<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_OOZIE" }) }>
								Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
							</Button>
						</div>
					</div>
				</div>
				<div className="row-fluid">
				    <div className="span6">
				        <div className="widget-box">
				        	<div className="widget-title"> <span className="icon"> <i className="icon-align-justify"></i> </span>
								<h5>Last 24 Hours Job Status Rate</h5>
							</div>
							<div className="widget-content nopadding">
								<br></br>
								<center>{ this.state.dailyRateChart }</center>
								<br></br>
							</div>
						</div>
						<div className="widget-box">
							<div className="widget-title"> <span className="icon"> <i className="icon-align-justify"></i> </span>
								<h5>Last 24 Hours Job Submitted</h5>
							</div>
							<div className="widget-content nopadding">
								<br></br>
								<center>{ this.state.dailySubmissionChart }</center>
								<br></br>
							</div>
						</div>
					</div>
					<div className="span6">
						<div className="widget-box">
							<div className="widget-title"> <span className="icon"> <i className="icon-align-justify"></i> </span>
								<h5>Last 7 Days Job Status Rate</h5>
							</div>
							<div className="widget-content nopadding">
								<br></br>
								<center>{ this.state.weeklyRateChart }</center>
								<br></br>
							</div>
						</div>
						<div className="widget-box">
							<div className="widget-title"> <span className="icon"> <i className="icon-align-justify"></i> </span>
								<h5>Last 7 Days Job Submitted</h5>
							</div>
							<div className="widget-content nopadding">
								<br></br>
								<center>{ this.state.weeklySubmissionChart }</center>
								<br></br>
							</div>
						</div>
					</div>
				</div>
				<div className="row-fluid">
					<div className="span12">
						<div className="widget-box">
							<div className="widget-title"> <span className="icon"> <i className="icon-align-justify"></i> </span>
								<h5>Your All Time Job Status Rate</h5>
							</div>
							<div className="widget-content nopadding">
								<br></br>
								<center>{ this.state.allTimeRateChart }</center>
								<br></br>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}