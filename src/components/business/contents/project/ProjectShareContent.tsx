import React from 'react';
import _isEqual from 'lodash/isEqual';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import UserManagementService from './../../../../services/UserManagementService';
import ProjectService from './../../../../services/ProjectService';

export default class ProjectShareContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			pageOfData: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_SHARE" });


	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_SHARE": 
				UserManagementService.getGroupByUser(
					(success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 2: error = `Group ${ this.props.group.group_name } doesn't have any project at the moment.`
				            	}
				            	this.props.showAlert({ style: `danger`, title: `Failed to Get List of Group to Share Project with!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, null, this.props.credentials.user_id);
		      	break;
			case "STORE_SHARE":
				if (action.confirmed) {
					const formElement = document.getElementById("form-project-create");
					const formData = new FormData(formElement);
					ProjectService.postProject((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Project Stored Successfully!`,
					            		message: `Project named ${ feedback.data[0].project_name } has been successfully stored.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Store Project!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_PROJECT" });
			      	}, formData, this.props.credentials.user_id);
				} 
				break;
			default:
				return;
		}
	}

	shareFormatter = (cell, row) => (
		(row.config_id === "null") ? null : 
		<div>
			<Button
				bsStyle="success" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "SHARE_PROJECT", data: row }) } >
				Share Project
			</Button>
		</div>
	);

	inventoryFormatter = (cell, row) => (
		(row.config_id === "null") ? null : 
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_INVENTORY", data: row }) } >
				View or Add Inventory
			</Button> 
		</div>
	);

	jobFormatter = (cell, row) => (
		(row.config_id === "null") ? null : 
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_JOB", data: row }) } >
				View Job Sumarry
			</Button>
		</div>
	);

	actionsFormatter = (cell, row) => (
		(row.config_id === "null") ? null : 
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_PROJECT", data: row }) } >
				Edit
			</Button> &nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_PROJECT", data: row }) } >
				Delete
			</Button>
		</div>
	);

	render() {
		const tableOptions = {
			noDataText: ("undefined" === typeof this.props.group || null === this.props.group) ? (<center>It seems that you don't have any project at the moment.</center>) : (<center>It seems that group { this.props.group.group_name } doesn't have any project at the moment.</center>),
		};
		return (
			<div id="project-content">
				<div className="row-fluid">
					<div className="span12" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_PROJECT" }) }>
							Add Project <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	{ ("undefined" === typeof this.props.group || null === this.props.group) ? (<h5>My Project List</h5>) : (<h5>Project List</h5>) }
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn 
							        	dataField = "project_name" isKey
							        	headerAlign = "center" dataSort >
							        	Project Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_share"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.shareFormatter }  >
							        	Sharing is Caring
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_inventory"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.inventoryFormatter }  >
							        	Inventory
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_job_history"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.jobFormatter }  >
							        	Job History
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-project-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_PROJECT", confirmed: true})
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="project_name"
										placeholder="Name"
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_PROJECT", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_PROJECT", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Edit Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-project-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_PROJECT", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="project_id" 
								defaultValue={ this.state.modal.edit.data.project_id } />
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="project_name"
										placeholder="Name" 
										defaultValue={ this.state.modal.edit.data.project_name } 
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_PROJECT", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_PROJECT", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete project named <strong>{ this.state.modal.delete.data.project_name }</strong>?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_PROJECT", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_PROJECT", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}