import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import ProjectService from './../../../../services/ProjectService';
import GenerateJarService from './../../../../services/GenerateJarService';
import OozieService from './../../../../services/OozieService';

export default class ProjectGeneratedJARContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	submit: { show: false, data: {} },
			},
			pageOfData: [],
			renderedInputArgs: [],
			renderedOutputArgs: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_GENERATED_JAR" });
	componentDidUpdate = (prevProps, prevState) => ( this.props.project !== prevProps.project ) ? this.dispatchAction({ type: "LOAD_GENERATED_JAR" }) : null;

	modalHandler(modalName: string = null, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	submit: { show: isShown, data: data },
					}
				});
				break;
			default:
				this.setState({
					modal: {
					 	submit: { show: false, data: {} },
					}
				});
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_GENERATED_JAR": 
				GenerateJarService.getJar((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
		        			console.log(feedback)
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 1: error = `The project ${ this.props.project.project_name } doesn't have any inventory compiled at the moment.`; break;
				            		default: break;
				            	}
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Generated JAR!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, null, this.props.project.project_id);
		      	break;
			case "CREATE_SUBMIT":
				ProjectService.getProjectInventory((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = action.data;
				            	data.inventory_name = feedback.data.inventory_name;
				            	this.modalHandler("submit", true, data);
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Prepare Job Submission!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, this.props.project.project_id);
		      	break;
		    case "DOWNLOAD_JAR":
		    	const downloadLink = `${ process.env.REACT_APP_REST_GENERATE_JAR }/${ this.props.credentials.user_apikey }/download_jar/${ action.data.compile_id }/project/${ action.data.project_id }`;
		    	window.open(downloadLink, '_blank');
				break;
			case "STORE_SUBMIT": 
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-submit-create");
					let formData = new FormData(formElement);
					OozieService.submitJob((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Job Submitted Successfully!`,
					            		message: `The oozie job has been successfully submitted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Submit Job!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.props.onPageChange('oozie');
			      	}, formData);
				} 
				break;
			default:
				return;
		}
	}

	typeFormatter = (cell, row) => (
		<a title="Download Compiled JAR" className="btn btn-link btn-mini"
			onClick={ () => this.dispatchAction({ type: "DOWNLOAD_JAR", data: row }) } >
			{ cell }
		</a>
	);


	actionsFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="info" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "CREATE_SUBMIT", data: row }) } >
				Submit Job
			</Button>
		</div>
	);

	renderNewArgs(type) {
		switch (type) {
			case "input": return (
							<div className="control-group">
								<label className="control-label">Addtnl Input:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_args_input"
										placeholder={ `Additional Input Arguement ${ this.state.renderedInputArgs.length + 1 }` }
										reaquired />
									<Button
										bsStyle="danger"
										onClick={ () => {
											const renderedInputArgs = this.state.renderedInputArgs, key = this.state.renderedInputArgs.length;
											if (Array.isArray(renderedInputArgs)) delete renderedInputArgs[key];
										}} >
										Delete
									</Button>
								</div>
							</div>
			);
			case "output": return (
							<div className="control-group">
								<label className="control-label">Addtnl Output:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_args_output"
										placeholder={ `Additional Output Arguement ${ this.state.renderedOutputArgs.length + 1 }` }
										reaquired />
									<Button
										bsStyle="danger"
										onClick={ () => {
											const renderedOutputArgs = this.state.renderedOutputArgs, key = this.state.renderedOutputArgs.length;
											if (Array.isArray(renderedOutputArgs)) delete renderedOutputArgs[key];
										}} >
										Delete
									</Button>
								</div>
							</div>
			);
			default: return null;
		}
	}

	render() {
		const tableOptions = {
			noDataText: (<center>It seems that project { this.props.project.project_name } doesn't have any inventory compiled into JAR at the moment.</center>),
		};
		return (
			<div id="project-content">
				<div className="row-fluid">
					<div className="span12" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_GENERATED_JAR" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>List of Compiled JAR File(s)</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn 
							        	dataField = "project_name" isKey
							        	headerAlign = "center" dataSort >
							        	Compile ID
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Path
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort  dataFormat = { this.typeFormatter } >
							        	Type
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.submit.show } onHide = { () => { this.modalHandler("submit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Submit Job</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-submit-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_SUBMIT", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="submit_project_id" 
								defaultValue={ this.props.project.project_id } />
							<div className="control-group">
								<label className="control-label">Compile Path:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_jar_path"
										value={ this.state.modal.submit.data.compile_path + this.state.modal.submit.data.project_name + `.jar` }
										readOnly />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Job Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_job_name"
										placeholder="Job Name"
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Class Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_class_name"
										value={ this.state.modal.submit.data.project_name + `.` + this.state.modal.submit.data.inventory_name }
										readOnly />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Input Args:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_args_input"
										placeholder="Input Arguement"
										reaquired />
									<Button
										bsStyle="primary"
										onClick={ () => {
											const renderedInputArgs = this.state.renderedInputArgs;
											if (Array.isArray(renderedInputArgs)) renderedInputArgs.push(this.renderNewArgs("input"));
										}} >
										Add More
									</Button>
								</div>
							</div>
							{ this.state.renderedInputArgs }
							<div className="control-group">
								<label className="control-label">Ouput Args:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="submit_args_output"
										placeholder="Output Arguement"
										reaquired />
									<Button
										bsStyle="primary"
										onClick={ () => {
											const renderedOutputArgs = this.state.renderedOutputArgs;
											if (Array.isArray(renderedOutputArgs)) renderedOutputArgs.push(this.renderNewArgs("output"));
										}} >
										Add More
									</Button>
								</div>
							</div>
							{ this.state.renderedOutputArgs }
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_SUBMIT", confirmed: true}) } >
							Submit Job
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_SUBMIT", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}