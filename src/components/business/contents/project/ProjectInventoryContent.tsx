import React from 'react';
import xml2js from 'xml2js';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import ProjectService from './../../../../services/ProjectService';
import GenerateJarService from './../../../../services/GenerateJarService';

export default class ProjectInventoryContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	generatejar: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			},
			pageOfData: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_INVENTORY" });
	componentDidUpdate = (prevProps, prevState) => ( this.props.project !== prevProps.project ) ? this.dispatchAction({ type: "LOAD_INVENTORY" }) : null;

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	generatejar: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					}
				});
				break;
			case "generatejar":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	generatejar: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	generatejar: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_INVENTORY": 
				ProjectService.getProjectInventory((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Inventory!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, this.props.project.project_id);
		      	break;
			case "CREATE_INVENTORY":
				this.modalHandler("create", true);
				break;
			case "ASK_GENERATE_JAR":
				this.modalHandler("generatejar", true, action.data);
				break;
		    case "DOWNLOAD_INVENTORY":
		    	const downloadLink = `${ process.env.REACT_APP_REST_PROJECT }/${ this.props.credentials.user_apikey }/download_inventory/${ action.data.inventory_id }/project/${ action.data.project_id }`;
		    	window.open(downloadLink, '_blank');
		    	break;
			case "DELETE_INVENTORY":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_INVENTORY":
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-inventory-create");
					const formData = new FormData(formElement);
					const csdFile = formData.get("inventory_file"); formData.delete("inventory_file");
					const fileReader = new FileReader();
					fileReader.readAsBinaryString(csdFile);
					fileReader.onloadend = () => {
						const xmlParser = new xml2js.Parser();
						const csdData = fileReader.result;
						xmlParser.parseString(csdData, (error, result) => {
							if (error) {
								this.props.showAlert({ 
									style: `danger`, 
									title: `Failed to Store Inventory!`,
									message: `Inventory file parsing failed: ${ error }`
								});
							} else {
								let isDataValid = false;
								if (result.hasOwnProperty("HGrid247")) {
									if (result.HGrid247.hasOwnProperty("$")) {
										isDataValid = true;
										const inventoryPackage = result;
										const version = inventoryPackage.HGrid247.$.version;
										delete inventoryPackage.HGrid247.$;
										inventoryPackage.HGrid247.version = version;
										const postData = {
											project_id: formData.get("project_id"),
											inventory_name: formData.get("inventory_name"),
											inventory_type: formData.get("inventory_type"),
											inventory_package: inventoryPackage,
										};
										ProjectService.postProjectInventory((success: boolean, request: Request, response: any) => {
									      	if (success) {
									        	try {
									        		const feedback = JSON.parse(response);
									        		if (feedback.err_code === 0) {
									        			this.props.showAlert({ 
									        				style: `success`, 
											            	title: `Inventory Stored Successfully!`,
											            	message: `Inventory named ${ feedback.data[0].inventory_name } has been successfully stored.`}
											            );
													} else {
														const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
														this.props.showAlert({ 
															style: `danger`, 
															title: `Failed to Store Inventory!`,
															message: `${error}`}
														);
													}
												} catch (exception) {
													this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
												}
											} else {
												this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
											}
											this.dispatchAction({ type: "LOAD_INVENTORY" });
										}, postData, this.props.project.project_id);
									} 
								} 
								if (!isDataValid) {
									this.props.showAlert({ 
										style: `danger`, 
										title: `Failed to Store Inventory!`,
										message: `The file is not a valid inventory file!`
									});
								} 
							}
						});
					}
				} 
				break;
			case "DO_GENERATE_JAR":
				const inventoryIdGenerateJar = this.state.modal.generatejar.data.inventory_id; 
				this.modalHandler("generatejar");
				if (action.confirmed) {
					GenerateJarService.generateJar((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
			        			console.log(feedback);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `JAR Generated Successfully!`,
					            		message: `The JAR file has been successfully generated.`}
					            	);
					            } else {
					            	let error = ``;
					            	if (feedback.err_msg && "object" === typeof feedback.err_msg) {
					            		for (let key in feedback.err_msg) {
					            			if (feedback.err_msg.hasOwnProperty(key)) {
					            				error = `${ error }${ key }: ${ feedback.err_msg[key] }; `;
					            			}
					            		}
					            	} else if (feedback.err_msg) error = feedback.err_msg;
					            	else error = `Unknown error.`
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Generate JAR!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_INVENTORY" });
			      	}, this.props.project.project_id, inventoryIdGenerateJar);
				} 
				break;
			case "DESTROY_INVENTORY":
				const inventoryIdDestroy = this.state.modal.delete.data.inventory_id;
				console.log(this.state.modal.delete.data);
				this.modalHandler("delete");
				if (action.confirmed) {
					ProjectService.deletProjectInventory((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Inventory Deleted Successfully!`,
					            		message: `Inventory has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete Inventory!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_INVENTORY" });
			      	}, this.props.project.project_id, inventoryIdDestroy);
				} 
				break;
			default:
				return;
		}
	}

	typeFormatter = (cell, row) => (
		<a title="Download CSD" className="btn btn-link btn-mini"
			onClick={ () => this.dispatchAction({ type: "DOWNLOAD_INVENTORY", data: row }) } >
			{ cell }
		</a>
	);

	actionsFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "ASK_GENERATE_JAR", data: row }) } >
				Generate JAR
			</Button> &nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_INVENTORY", data: row }) } >
				Delete
			</Button>
		</div>
	);

	render() {
		const tableOptions = {
			noDataText: <center>It seems that project { this.props.project.project_name } doesn't have any inventory at the moment.</center>,
		};
		return (
			<div id="project-content">
				<div className="row-fluid">
					<div className="span6" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_INVENTORY" }) }>
							Refresh Data <span className="plus-link"><i className=" icon-refresh"></i></span>
						</Button>
					</div>
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_INVENTORY" }) }>
							Add Inventory <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>Inventory List</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn 
							        	dataField = "inventory_name" isKey
							        	headerAlign = "center" dataSort >
							        	Inventory Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "inventory_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "inventory_path"
							        	headerAlign = "center" dataSort >
							        	Path
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "inventory_type"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.typeFormatter } >
							        	Type
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "inventory_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create Inventory</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-inventory-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_INVENTORY", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="project_id" 
								defaultValue={ this.props.project.project_id } />
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="inventory_name"
										placeholder="Name"
										required />
								</div>
							</div>
							<FormControl 
								type="hidden"
								name="inventory_type" 
								defaultValue="csd" />
							<div className="control-group">
								<label className="control-label">CSD File:</label>
								<div className="controls">
									<FormControl 
										type="file"
										name="inventory_file"
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_INVENTORY", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_INVENTORY", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.generatejar.show } onHide = { () => { this.modalHandler("generatejar") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Generate JAR from Inventory</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>
							This will generate JAR from the inventory { this.state.modal.generatejar.data.inventory_name }. It may take a minute to complete.<br></br>
							<br></br>
							<strong>Proceed?</strong>
						</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DO_GENERATE_JAR", confirmed: true}) } >
							Yes
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DO_GENERATE_JAR", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Inventory</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete inventory named <strong>{ this.state.modal.delete.data.inventory_name }</strong>?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_INVENTORY", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_INVENTORY", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}