import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import UserManagementService from './../../../../services/UserManagementService';
import ProjectService from './../../../../services/ProjectService';

export default class ProjectContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	edit: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			 	share: { show: false, data: {} },
			 	unshare: { show: false, data: {} },
			},
			pageOfData: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_PROJECT" });
	componentDidUpdate = (prevProps, prevState) => ( this.props.group !== prevProps.group ) ? this.dispatchAction({ type: "LOAD_PROJECT" }) : null;

	modalHandler(modalName: string = null, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					 	share: { show: false, data: {} },
					 	unshare: { show: false, data: {} },
					}
				});
				break;
			case "edit":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
					 	share: { show: false, data: {} },
					 	unshare: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
					 	share: { show: false, data: {} },
					 	unshare: { show: false, data: {} },
					}
				});
				break;
			case "share":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					 	share: { show: isShown, data: data },
					 	unshare: { show: false, data: {} },
					}
				});
				break;
			case "unshare":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					 	share: { show: false, data: {} },
					 	unshare: { show: isShown, data: data },
					}
				});
				break;
			default:
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					 	share: { show: false, data: {} },
					 	unshare: { show: false, data: {} },
					}
				});
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_PROJECT": 
				if ("undefined" === typeof this.props.group || null === this.props.group) ProjectService.getProjectByUser(
					(success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 2: error = `You don't have any project at the moment.`; break;
				            		default: break;
				            	}
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Project!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, null, this.props.credentials.user_id);
				else ProjectService.getProjectByGroup(
					(success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = [];
				            	if (Array.isArray(feedback.data)) feedback.data.map((value, index, array) => {
				            		if (value.project_is_share === "true") data.push(value);
				            		return value;
				            	});
				            	this.setState({ data: data })
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 2: error = `Group ${ this.props.group.group_name } doesn't have any project at the moment.`; break;
				            		default: break;
				            	}
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Project!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, null, this.props.group.group_id);
		      	break;
			case "VIEW_INVENTORY":
				this.props.onPageChange("project-inventory", { project: action.data });
				break;
			case "VIEW_GENERATED_JAR":
				this.props.onPageChange("project-generatedjar", { project: action.data });
				break;
			case "VIEW_JOB":
				this.props.onPageChange("oozie-project", { project: action.data });
				break;
			case "CREATE_PROJECT":
				this.modalHandler("create", true);
				break;
			case "EDIT_SHARE":
				UserManagementService.getGroupByUser((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = {
				            		project_id: action.data.project_id,
				            		project_name: action.data.project_name,
				            		project_is_share: (action.data.project_is_share === "true"),
				            		groups: feedback.data,
				            	}
				            	if (!data.project_is_share) this.modalHandler("share", true, data);
				            	else this.modalHandler("unshare", true, data);
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to List of Group to Share Project With!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, null, this.props.credentials.user_id);
				break;
			case "EDIT_PROJECT":
				this.modalHandler("edit", true, action.data);
				break;
			case "DELETE_PROJECT":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_PROJECT": 
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-project-create");
					let formData = new FormData(formElement);
					ProjectService.postProject((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Project Stored Successfully!`,
					            		message: `Project named ${ feedback.data.project_name } has been successfully stored.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Store Project!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_PROJECT" });
			      	}, formData, this.props.credentials.user_id);
				} 
				break;
			case "UPDATE_PROJECT":
				this.modalHandler("edit");
				if (action.confirmed) {
					const formElement = document.getElementById("form-project-edit");
					const formData = new FormData(formElement);
					ProjectService.putProject((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Project Updated Successfully!`,
					            		message: `Project named ${ feedback.data.project_name } has been successfully updated.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Project!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_PROJECT" });
			      	}, formData, formData.get("project_id"), this.props.credentials.user_id);
				} 
				break;
			case "UPDATE_SHARE":
				this.modalHandler();
				if (action.confirmed) {
					const formData = new FormData();
					formData.append("project_is_share", action.data.project_is_share ? "false": "true");
					const projectId = action.data.project_id;
					const groupId = (action.data.project_is_share) ? 
						action.data.groups[0].group_id : 
						new FormData(document.getElementById("form-share-edit")).get("group_id");
					ProjectService.shareProject((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	const message = 
						            	feedback.status ? 
						            		feedback.status : 
						            		action.data.project_is_share ? 
						            			`Project named ${ action.data.project_name } has been successfully unshared.` : 
						            			`Project named ${ action.data.project_name } has been successfully shared.`;
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: action.data.project_is_share ? `Project Unshared Successfully!` : `Project Shared Successfully!`,
					            		message: message,
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: action.data.project_is_share ? `Failed to Unshare Project!` : `Failed to Share Project!`,
					            		message: `${ error }`
					            	});
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_PROJECT" });
			      	}, formData, projectId, groupId);
				} 
				break;
			case "DESTROY_PROJECT":
				const projectId = this.state.modal.delete.data.project_id;
				this.modalHandler("delete");
				if (action.confirmed) {
					ProjectService.deleteProject((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Project Deleted Successfully!`,
					            		message: `Project has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete Project!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_PROJECT" });
			      	}, projectId);
				} 
				break;
			default:
				return;
		}
	}

	shareFormatter = (cell, row) => (
		(row.project_is_share === "false") ? (
		<div>
			<Button
				bsStyle="success" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_SHARE", data: row }) } >
				Share Project
			</Button>
		</div>
		) : (
		<div>
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_SHARE", data: row }) } >
				Unshare Project
			</Button>
		</div>
		)
	);

	generatedJARFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_GENERATED_JAR", data: row }) } >
				View Generated JAR
			</Button> 
		</div>
	);

	inventoryFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_INVENTORY", data: row }) } >
				View or Add Inventory
			</Button> 
		</div>
	);

	jobFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_JOB", data: row }) } >
				View Job Sumarry
			</Button>
		</div>
	);

	actionsFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_PROJECT", data: row }) } >
				Edit
			</Button> &nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_PROJECT", data: row }) } >
				Delete
			</Button>
		</div>
	);

	renderGroupList = () => {
		const groups = ("undefined" !== typeof this.state.modal.share.data.groups) ? this.state.modal.share.data.groups : [];
		const options = [];
		groups.map((value, index, array) => {
			return options.push(
				<option key={ value.group_id.toString() } value={ value.group_id }>
					{ value.group_name }
				</option>);
		});
		return options;
	}

	render() {
		const tableOptions = {
			noDataText: ("undefined" === typeof this.props.group || null === this.props.group) ? (<center>It seems that you don't have any project at the moment.</center>) : (<center>It seems that group { this.props.group.group_name } doesn't have any project that shared to at the moment.</center>),
		};
		return (
			<div id="project-content">
				<div className="row-fluid">
					<div className={ ("undefined" === typeof this.props.group || null === this.props.group) ? "span6" : "span12" } style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_PROJECT" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
					{ ("undefined" === typeof this.props.group || null === this.props.group) ? (
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_PROJECT" }) }>
							Add Project <span className="plus-link"><i className="icon-plus"></i></span>
						</Button>
					</div>
					) : null }
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	{ ("undefined" === typeof this.props.group || null === this.props.group) ? (<h5>My Project List</h5>) : (<h5>Project List</h5>) }
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn 
							        	dataField = "project_name" isKey
							        	headerAlign = "center" dataSort >
							        	Project Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_share"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.shareFormatter }  >
							        	Sharing is Caring
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_inventory"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.inventoryFormatter }  >
							        	Inventory
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_generated_jar"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.generatedJARFormatter }  >
							        	Generated JAR
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_job_history"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.jobFormatter }  >
							        	Job History
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "project_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-project-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_PROJECT", confirmed: true})
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="project_name"
										placeholder="Name"
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_PROJECT", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_PROJECT", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Edit Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-project-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_PROJECT", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="project_id" 
								defaultValue={ this.state.modal.edit.data.project_id } />
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="project_name"
										placeholder="Name" 
										defaultValue={ this.state.modal.edit.data.project_name } 
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_PROJECT", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_PROJECT", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete project named <strong>{ this.state.modal.delete.data.project_name }</strong>?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_PROJECT", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_PROJECT", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>

				<Modal show = { this.state.modal.share.show } onHide = { () => { this.modalHandler("share") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Share Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{ "undefined" !== typeof this.state.modal.share.data.groups ? this.state.modal.share.data.groups.length > 0 ? (
						<Form
							id = "form-share-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_SHARE", confirmed: true, data: this.state.modal.share.data })
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">Share to Group:</label>
								<div className="controls">
									<FormControl 
										componentClass="select"
										name="group_id" >
										{ this.renderGroupList() }
									</FormControl>
								</div>
							</div>
						</Form>
						) : (
						<font size={2}>You don't have any group at the moment to share this project with.</font>
						) : null }
					</Modal.Body>
					<Modal.Footer>
						{ "undefined" !== typeof this.state.modal.share.data.groups ? this.state.modal.share.data.groups.length > 0 ? (
						<div>
							<Button
								bsStyle="primary"
								onClick={ () => this.dispatchAction({type: "UPDATE_SHARE", confirmed: true, data: this.state.modal.share.data }) } >
								Share Project
							</Button>
							<Button
								bsStyle="danger"
								onClick={ () => this.dispatchAction({type: "UPDATE_SHARE", confirmed: false, data: this.state.modal.share.data }) } >
								Keep It Private
							</Button>
						</div>
						) : "Baciro Gateway" : null }
					</Modal.Footer>
				</Modal>

				<Modal show = { this.state.modal.unshare.show } onHide = { () => { this.modalHandler("unshare") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Unshare Project</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						{ "undefined" !== typeof this.state.modal.unshare.data.groups ? this.state.modal.unshare.data.groups.length > 0 ? (
						<font size={2}>Project <strong>{ this.state.modal.delete.data.project_name }</strong> is being shared to a group. <strong>Are you sure to unshare this project?</strong></font>
						) : (
						<font size={2}>You don't have any group at the moment to unshare this project with.</font>
						) : null }
					</Modal.Body>
					<Modal.Footer>
						{ "undefined" !== typeof this.state.modal.unshare.data.groups ? this.state.modal.unshare.data.groups.length > 0 ? (
						<div>
							<Button
								bsStyle="danger"
								onClick={ () => this.dispatchAction({type: "UPDATE_SHARE", confirmed: true, data: this.state.modal.unshare.data }) } >
								Yes, I'm Sure
							</Button>
							<Button
								bsStyle="primary"
								onClick={ () => this.dispatchAction({type: "UPDATE_SHARE", confirmed: false, data: this.state.modal.unshare.data }) } >
								No, Keep Sharing
							</Button>
						</div>
						) : "Baciro Gateway" : null }
					</Modal.Footer>
				</Modal>

			</div>
		);
	}

}