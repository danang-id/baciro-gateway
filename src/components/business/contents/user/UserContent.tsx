import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import UserManagementService from './../../../../services/UserManagementService';

export default class UserContent extends React.Component {
	
	private state: any;
	private props: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	edit: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			 	password: { show: false, data: {} },
			 	ipaddress: { show: false, data: {} },
			},
			pageOfData: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_USER" });

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			password: { show: false, data: {} },
			 			ipaddress: { show: false, data: {} },
					}
				});
				break;
			case "edit":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
			 			password: { show: false, data: {} },
			 			ipaddress: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
			 			password: { show: false, data: {} },
			 			ipaddress: { show: false, data: {} },
					}
				});
				break;
			case "password":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			password: { show: isShown, data: data },
			 			ipaddress: { show: false, data: {} },
					}
				});
				break;
			case "ipaddress":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
			 			password: { show: false, data: {} },
			 			ipaddress: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_USER": 
				UserManagementService.getUser((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load User!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	});
		      	break;
			case "CREATE_USER":
				this.modalHandler("create", true);
				break;
			case "EDIT_USER":
				this.modalHandler("edit", true, action.data);
				break;
			case "EDIT_IPADDRESS":
				this.modalHandler("ipaddress", true, action.data);
				break;
			case "EDIT_PASSWORD":
				this.modalHandler("password", true, action.data);
				break;
			case "DELETE_USER":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_USER":
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-user-create");
					const formData = new FormData(formElement);
					UserManagementService.postUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `User Stored Successfully!`,
					            		message: `User ${ feedback.data[0].user_firstname } ${ feedback.data[0].user_lastname } has been successfully stored.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Store User!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_USER" });
			      	}, formData, formData.get("user_id"));
				} 
				break;
			case "UPDATE_USER":
				this.modalHandler("edit");
				if (action.confirmed) {
					const formElement = document.getElementById("form-user-edit");
					const formData = new FormData(formElement);
					UserManagementService.putUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `User Updated Successfully!`,
					            		message: `User ${ feedback.data[0].user_firstname } ${ feedback.data[0].user_lastname } has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update User!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_USER" });
			      	}, formData, formData.get("user_id"));
				} 
				break;

			case "UPDATE_IPADDRESS":
				this.modalHandler("ipaddress");
				if (action.confirmed) {
					const formElement = document.getElementById("form-ipaddress-edit");
					const formData = new FormData(formElement);
					UserManagementService.putUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `IP Address Added Successfully!`,
					            		message: `A new IP address of ${ feedback.data[0].user_firstname } ${ feedback.data[0].user_lastname } has been successfully added.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Add IP Address!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_USER" });
			      	}, formData, formData.get("user_id"));
				} 
				break;
			case "UPDATE_PASSWORD":
				this.modalHandler("password");
				if (action.confirmed) {
					const formElement = document.getElementById("form-password-edit");
					const formData = new FormData(formElement);
					UserManagementService.putUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Password Updated Successfully!`,
					            		message: `Password of ${ feedback.data[0].user_firstname } ${ feedback.data[0].user_lastname } has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Password!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_USER" });
			      	}, formData, formData.get("user_id"));
				} 
				break;
			case "DESTROY_USER":
				const userId = this.state.modal.delete.data.user_id;
				this.modalHandler("delete");
				if (action.confirmed) {
					UserManagementService.deleteUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `User Deleted Successfully!`,
					            		message: `User has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete User!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_USER" });
			      	}, userId);
				} 
				break;
			default:
				return;
		}
	}

	fullNameFormatter = (cell, row) => `${row.user_firstname} ${row.user_lastname}`;

	roleFormatter = (cell, row) => (cell === 1) ? `Administrator` : `Operator`;

	actionsFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_USER", data: row }) } >
				Edit
			</Button> &nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_USER", data: row }) } >
				Delete
			</Button>
		</div>
	);

	renderList = (list: any, separator: string = ",") => {
		let elements = [];
		const itemList = list.split(separator);
		for (let item of itemList) elements.push(<li>{item}</li>);
		return (<ul className="nav nav-tabs nav-stacked">{elements}</ul>)
	}

	render() {
		const tableOptions = {
			noDataText: (<center>It seems there is no any users at the moment. But, this should NEVER happens. If this is happens, who are you to access this page?</center>),
		};
		return (
			<div id="user-content">
				<div className="row-fluid">
					<div className="span6" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_USER" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_USER" }) }>
							Add User <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>User List</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn
							        	dataField = "user_fullname" isKey
							        	headerAlign = "center" dataSort dataFormat = { this.fullNameFormatter }>
							        	Full Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "user_email"
							        	headerAlign = "center" dataSort >
							        	Email Address
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "user_role_id"
							        	headerAlign = "center" dataAlign = "center" dataSort dataFormat = { this.roleFormatter } >
							        	Role
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "user_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "user_update_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Last Updated At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "user_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
			      			</div>
			      			<Pagination className="pull-right" items={this.state.data} onChangePage={
			      				(pageOfData: Array<any>) => { 
			      					this.dispatchAction({
			      						type: "PAGE_CHANGE",
			      						pageOfData: pageOfData,
			      					}); 
			      				}
			      			} />
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create User</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-user-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_USER", confirmed: true})
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">First Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_firstname"
										placeholder="First Name" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Last Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_lastname"
										placeholder="Last Name" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Email Address:</label>
								<div className="controls">
									<FormControl 
										type="email"
										name="user_email"
										placeholder="someone@solusi247.com" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Password:</label>
								<div className="controls">
									<FormControl 
										type="password"
										name="user_password"
										placeholder="Something Secret" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">IP Address:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_ip_address"
										placeholder="nnn.nnn.nnn.nnn" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Role:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="user_role_id"
										defaultValue={2}
										required>
										<ToggleButton value={1}>Administrator</ToggleButton>
										<ToggleButton value={2}>Operator</ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_USER", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_USER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Edit User</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-user-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_USER", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.modal.edit.data.user_id } />
							<div className="control-group">
								<label className="control-label">First Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_firstname"
										placeholder="First Name" 
										defaultValue={ this.state.modal.edit.data.user_firstname }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Last Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_lastname"
										placeholder="Last Name" 
										defaultValue={ this.state.modal.edit.data.user_lastname }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Email Address:</label>
								<div className="controls">
									<FormControl 
										type="email"
										name="user_email"
										placeholder="someone@solusi247.com" 
										defaultValue={ this.state.modal.edit.data.user_email }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Role:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="user_role_id"
										defaultValue={this.state.modal.edit.data.user_role_id}
										required>
										<ToggleButton value={1}>Administrator</ToggleButton>
										<ToggleButton value={2}>Operator</ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">IP Addresses:</label>
								<div className="controls">
									{ ("undefined" !== typeof this.state.modal.edit.data.user_ip_address) ? this.renderList(this.state.modal.edit.data.user_ip_address) : "" }
								</div>
							</div>
							<div className="control-group">
								<label className="control-label"></label>
								<div className="controls">
									<Button
										bsStyle="info"
										onClick={ () => { 
											const row = this.state.modal.edit.data;
											this.dispatchAction({type: "UPDATE_USER", confirmed: false});
											this.dispatchAction({ type: "EDIT_IPADDRESS", data: row });
										} } >
										Add New IP Address
									</Button>
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Password:</label>
								<div className="controls">
									<Button
										bsStyle="danger"
										onClick={ () => { 
											const row = this.state.modal.edit.data;
											this.dispatchAction({type: "UPDATE_USER", confirmed: false});
											this.dispatchAction({ type: "EDIT_PASSWORD", data: row });
										} } >
										Change Password
									</Button>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_USER", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_USER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.ipaddress.show } onHide = { () => { this.modalHandler("ipaddress") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Change Password for { this.state.modal.ipaddress.data.user_firstname } { this.state.modal.ipaddress.data.user_lastname }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-ipaddress-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_IPADDRESS", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.modal.ipaddress.data.user_id } />
							<div className="control-group">
								<label className="control-label">Add IP Address:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_ip_address"
										placeholder="nnn.nnn.nnn.nnn" 
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_IPADDRESS", confirmed: true}) } >
							Add New IP Address
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_IPADDRESS", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.password.show } onHide = { () => { this.modalHandler("password") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Change Password for { this.state.modal.password.data.user_firstname } { this.state.modal.password.data.user_lastname }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-password-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_PASSWORD", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.modal.password.data.user_id } />
							<div className="control-group">
								<label className="control-label">New Password:</label>
								<div className="controls">
									<FormControl 
										type="password"
										name="user_password"
										placeholder="Something Secret" 
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_PASSWORD", confirmed: true}) } >
							Change Password
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_PASSWORD", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete User</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete user <strong>{ this.state.modal.delete.data.user_firstname } { this.state.modal.delete.data.user_lastname }</strong>?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_USER", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_USER", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}