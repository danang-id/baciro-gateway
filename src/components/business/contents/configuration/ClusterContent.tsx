import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import ClusterService from './../../../../services/ClusterService';

export default class ClusterContent extends React.Component {
	
	private state: any;
	private props: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	edit: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			},
			pageOfData: [],
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_CLUSTER" });

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					}
				});
				break;
			case "edit":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_CLUSTER": 
				ClusterService.getCluster((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	this.setState({ data: data })
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Cluster!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	});
		      	break;
			case "VIEW_CONFIGURATION":
				this.props.onPageChange("cluster-configuration", { cluster: action.data });
				break;
			case "CREATE_CLUSTER":
				this.modalHandler("create", true);
				break;
			case "EDIT_CLUSTER":
				this.modalHandler("edit", true, action.data);
				break;
			case "DELETE_CLUSTER":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_CLUSTER":
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-cluster-create");
					const formData = new FormData(formElement);
					ClusterService.postCluster((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Cluster Stored Successfully!`,
					            		message: `Cluster ${ feedback.data[0].cluster_name } has been successfully stored.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Store Cluster!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_CLUSTER" });
			      	}, formData, formData.get("cluster_id"));
				} 
				break;
			case "UPDATE_CLUSTER":
				this.modalHandler("edit");
				if (action.confirmed) {
					const formElement = document.getElementById("form-cluster-edit");
					const formData = new FormData(formElement);
					ClusterService.putCluster((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Cluster Updated Successfully!`,
					            		message: `Cluster ${ feedback.data[0].cluster_name } has been successfully updated.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Cluster!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_CLUSTER" });
			      	}, formData, formData.get("cluster_id"));
				} 
				break;
			case "DESTROY_CLUSTER":
				const clusterId = this.state.modal.delete.data.cluster_id;
				this.modalHandler("delete");
				if (action.confirmed) {
					ClusterService.deleteCluster((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Cluster Deleted Successfully!`,
					            		message: `Cluster has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete Cluster!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_CLUSTER" });
			      	}, clusterId);
				} 
				break;
			default:
				return;
		}
	}

	statusFormatter = (cell, row) => {
		switch(cell) {
			case "default":
				return (<strong>Default Cluster</strong>);
			case "active":
				return (<font color="green">Active</font>);
			case "inactive":
				return (<font color="red">Not Active</font>);
			default:
				return ({ cell }); 
		}
	};

	configurationFormatter = (cell, row) => (
		<Button
			bsStyle="primary" className="btn-mini"
			onClick={ () => this.dispatchAction({ type: "VIEW_CONFIGURATION", data: row }) } >
			View Configuration
		</Button>
	);

	actionsFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_CLUSTER", data: row }) } >
				Edit
			</Button> &nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_CLUSTER", data: row }) } >
				Delete
			</Button>
		</div>
	);

	render() {
		const tableOptions = {
			noDataText: (<center>It seems that you don't have any cluster at the moment.</center>),
		};
		return (
			<div id="cluster-content">
				<div className="row-fluid">
					<div className="span6" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_CLUSTER" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_CLUSTER" }) }>
							Add Cluster <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>Cluster List</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn
							        	dataField = "cluster_name" isKey
							        	headerAlign = "center" dataSort >
							        	Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "cluster_status"
							        	headerAlign = "center" dataAlign = "center" dataSort dataFormat = { this.statusFormatter } >
							        	Status
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "cluster_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "cluster_update_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Last Updated At
							        </TableHeaderColumn>
							        <TableHeaderColumn 
							        	dataField = "cluster_configuration"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.configurationFormatter } >
							        	Configuration
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "cluster_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create Cluster</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-cluster-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_CLUSTER", confirmed: true})
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="cluster_name"
										placeholder="Cluster Name" 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Status:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="cluster_status"
										defaultValue="inactive" 
										required >
										<ToggleButton value={"default"}><strong>Default</strong></ToggleButton>
										<ToggleButton value={"active"}><font color="green">Active</font></ToggleButton>
										<ToggleButton value={"inactive"}><font color="red">Not Active</font></ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_CLUSTER", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_CLUSTER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Edit Cluster</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-cluster-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_CLUSTER", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="cluster_id" 
								defaultValue={ this.state.modal.edit.data.cluster_id } />
							<div className="control-group">
								<label className="control-label">Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="cluster_name"
										placeholder="Name" 
										defaultValue={ this.state.modal.edit.data.cluster_name } 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Status:</label>
								<div className="controls">
									<ToggleButtonGroup
										type="radio"
										name="cluster_status"
										defaultValue={this.state.modal.edit.data.cluster_status}
										required>
										<ToggleButton value={"default"}><strong>Default</strong></ToggleButton>
										<ToggleButton value={"active"}><font color="green">Active</font></ToggleButton>
										<ToggleButton value={"inactive"}><font color="red">Not Active</font></ToggleButton>
									</ToggleButtonGroup>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_CLUSTER", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_CLUSTER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Cluster</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete cluster <strong>{ this.state.modal.delete.data.cluster_name }</strong>?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_CLUSTER", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_CLUSTER", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}