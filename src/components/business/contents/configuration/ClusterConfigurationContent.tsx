import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import ClusterService from './../../../../services/ClusterService';

export default class ClusterConfigurationContent extends React.Component {
	
	private state: any;
	private props: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			modal: {
			 	create: { show: false, data: {} },
			 	edit: { show: false, data: {} },
			 	delete: { show: false, data: {} },
			},
			pageOfData: [],
		};
	}

	componentWillMount = () => { this.dispatchAction({ type: "LOAD_CONFIGURATION" }); };

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "create":
				this.setState({
					modal: {
					 	create: { show: isShown, data: data },
					 	edit: { show: false, data: {} },
					 	delete: { show: false, data: {} },
					}
				});
				break;
			case "edit":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: isShown, data: data },
					 	delete: { show: false, data: {} },
					}
				});
				break;
			case "delete":
				this.setState({
					modal: {
					 	create: { show: false, data: {} },
					 	edit: { show: false, data: {} },
					 	delete: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: object) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_CONFIGURATION": 
				ClusterService.getClusterConfiguration((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = [];
				            	if (Array.isArray(feedback.data)) feedback.data.map((value, index, array) => {
				            		if (value.config_id !== "null") data.push(value);
				            		return value;
				            	});
				            	this.setState({ data: data })
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Cluster Configuration!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, this.props.cluster.cluster_id);
		      	break;
			case "CREATE_CONFIGURATION":
				this.modalHandler("create", true);
				break;
			case "EDIT_CONFIGURATION":
				this.modalHandler("edit", true, action.data);
				break;
			case "DELETE_CONFIGURATION":
				this.modalHandler("delete", true, action.data);
				break;
			case "STORE_CONFIGURATION":
				this.modalHandler("create");
				if (action.confirmed) {
					const formElement = document.getElementById("form-cluster-configuration-create");
					const formData = new FormData(formElement);
					ClusterService.postClusterConfiguration((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Configuration Stored Successfully!`,
					            		message: `Configuration key ${ feedback.data[0].config_key } has been successfully stored.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Store Configuration!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_CONFIGURATION" });
			      	}, formData, this.props.cluster.cluster_id, formData.get("config_id"));
				} 
				break;
			case "UPDATE_CONFIGURATION":
				this.modalHandler("edit");
				if (action.confirmed) {
					const formElement = document.getElementById("form-cluster-configuration-edit");
					const formData = new FormData(formElement);
					ClusterService.putClusterConfiguration((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Configuration Updated Successfully!`,
					            		message: `Configuration key ${ feedback.data[0].config_key } has been successfully updated.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Configuration!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_CONFIGURATION" });
			      	}, formData, this.props.cluster.cluster_id, formData.get("config_id"));
				} 
				break;
			case "DESTROY_CONFIGURATION":
				const configurationId = this.state.modal.delete.data.config_id;
				this.modalHandler("delete");
				if (action.confirmed) {
					ClusterService.deleteClusterConfiguration((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Configuration Deleted Successfully!`,
					            		message: `Configuration has been successfully deleted.`}
					            	);
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Delete Configuration!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_CONFIGURATION" });
			      	}, this.props.cluster.cluster_id, configurationId);
				} 
				break;
			default:
				return;
		}
	}

	actionsFormatter = (cell, row) => (
		(row.config_id === "null") ? null : 
		<div>
			<Button
				bsStyle="primary" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "EDIT_CONFIGURATION", data: row }) } >
				Edit
			</Button> &nbsp;
			<Button
				bsStyle="danger" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "DELETE_CONFIGURATION", data: row }) } >
				Delete
			</Button>
		</div>
	);

	render() {
		const tableOptions = {
			noDataText: (<center>It seems that this cluster ({ this.props.cluster.cluster_name }) doesn't have any configuration at the moment.</center>),
		};
		return (
			<div id="cluster-configuration-content">
				<div className="row-fluid">
					<div className="span6" style={{ padding: "5px 0px 0px 0px" }}>
						<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_CONFIGURATION" }) }>
							Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
						</Button>
					</div>
					<div className="span6" style={{ padding: "5px 0px 10px 0px" }}>
						<Button bsStyle="success" className="pull-right" onClick={ () => this.dispatchAction({ type: "CREATE_CONFIGURATION" }) }>
							Add Configuration <span className="plus-link"><i className=" icon-plus"></i></span>
						</Button>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>Cluster { this.props.cluster.cluster_name }: Configuration List</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn 
							        	dataField = "config_id" isKey
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Configuration ID
							        </TableHeaderColumn>
							        <TableHeaderColumn 
							        	dataField = "config_key"
							        	headerAlign = "center" dataSort >
							        	Unique Key Name
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "config_value"
							        	headerAlign = "center" dataSort >
							        	Value
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "config_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "config_update_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Last Updated At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "cluster_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
				<Modal show = { this.state.modal.create.show } onHide = { () => { this.modalHandler("create") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Create Configuration for Cluster { this.props.cluster.cluster_name }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-cluster-configuration-create"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "STORE_CONFIGURATION", confirmed: true})
								}
							} } 
							horizontal>
							<div className="control-group">
								<label className="control-label">Unique Key Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="config_key"
										placeholder="Unique Key Name"
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Value:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="config_value"
										placeholder="Value" 
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "STORE_CONFIGURATION", confirmed: true}) } >
							Store
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "STORE_CONFIGURATION", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.edit.show } onHide = { () => { this.modalHandler("edit") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Edit Configuration for Cluster { this.props.cluster.cluster_name }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-cluster-configuration-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_CONFIGURATION", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="config_id" 
								defaultValue={ this.state.modal.edit.data.config_id } />
							<div className="control-group">
								<label className="control-label">Unique Key Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="config_key"
										placeholder="Unique Key Name" 
										defaultValue={ this.state.modal.edit.data.config_key } 
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Value:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="config_value"
										placeholder="Value" 
										defaultValue={ this.state.modal.edit.data.config_value }
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_CONFIGURATION", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_CONFIGURATION", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.delete.show } onHide = { () => { this.modalHandler("delete") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Delete Configuration for Cluster { this.props.cluster.cluster_name }</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<font size={2}>Are you sure to delete configuration key <strong>{ this.state.modal.delete.data.config_key }</strong> of cluster { this.props.cluster.cluster_name }?</font>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "DESTROY_CONFIGURATION", confirmed: true}) } >
							Yes, I'm Sure
						</Button>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "DESTROY_CONFIGURATION", confirmed: false}) } >
							No, Cancel Delete
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}