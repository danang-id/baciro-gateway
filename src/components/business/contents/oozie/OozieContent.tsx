import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import OozieService from './../../../../services/OozieService';

export default class OozieContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			pageOfData: [],
			statusFailingTime: 0,
			mounted: false,
			realtime: false,
		};
	}

	componentDidMount = () => {
		this.dispatchAction({ type: "LOAD_OOZIE" });
		this.setState({ mounted: true });
	}
	componentDidUpdate = (prevProps, prevState) => ( this.props.project !== prevProps.project ) ? this.dispatchAction({ type: "LOAD_OOZIE" }) : null;
	componentWillUnmount = () => {
		this.setState({ mounted: false });
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_OOZIE": 
				const projectPropAvailable = "undefined" === typeof this.props.project || null === this.props.project;
				if (projectPropAvailable) OozieService.getJobId((success: boolean, request: Request, response: any) => {
					if (!this.state.mounted) return;
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	if (Array.isArray(feedback.data)) data.map((value, index, array) => {
				            		if (this.state.realtime) {
				            			this.dispatchAction({ type: "LOAD_STATUS", data: value, status: value.oozie_status });
				            			value.oozie_status = `Getting status...`;
									}
									value.oozie_realtime = this.state.realtime;
				            		return value;
				            	});
				            	this.setState({ data: data });
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 2: error = `You don't have any oozie job recorded at the moment.`; break;
				            		default: break;
				            	}
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Oozie Job!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	});
				else OozieService.getJobHistory((success: boolean, request: Request, response: any) => {
					if (!this.state.mounted) return;
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data;
				            	if (Array.isArray(feedback.data)) data.map((value, index, array) => {
				            		if (this.state.realtime) {
				            			this.dispatchAction({ type: "LOAD_STATUS", data: value, status: value.oozie_status });
				            			value.oozie_status = `Getting status...`;
									}
									value.oozie_realtime = this.state.realtime;
				            		return value;
				            	});
				            	this.setState({ data: data });
				            } else {
				            	let error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	switch (feedback.err_code) {
				            		case 2: error = `The project ${ this.props.project.project_name } doesn't have any oozie job recorded at the moment.`; break;
				            		default: break;
				            	}
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load Oozie Job!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
				}, this.props.project.project_id);
		      	break;
		    case "LOAD_STATUS":
		    	if (action.data instanceof Object) {
		    		OozieService.getJobStatus((success: boolean, request: Request, response: any) => {
		    			if (!this.state.mounted) return;
		    			let fails = true;
		      			if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	fails = false;
					            	const data = this.state.data;
					            	if (Array.isArray(data)) data.map((value, index, array) => {
					            		if (value.oozie_job_id === action.data.oozie_job_id) {
					            			value.oozie_status = feedback.data.oozie_status;
					            			value.oozie_realtime = true;
					            		}
					            		return value;
					            	});
					            	this.setState({ data: data });
					            } else {
					            	const data = this.state.data;
					            	if (Array.isArray(data)) data.map((value, index, array) => {
					            		if (value.oozie_job_id === action.data.oozie_job_id) {
					            			value.oozie_status = action.status;
					            			value.oozie_realtime = false;
					            		}
					            		return value;
					            	});
					            	this.setState({ data: data });
					            	this.setState({ statusFailingTime: ++this.state.statusFailingTime });
					            }
					        } catch (exception) {
					        	const data = this.state.data;
					            if (Array.isArray(data)) data.map((value, index, array) => {
					            	if (value.oozie_job_id === action.data.oozie_job_id) {
					            		value.oozie_status = action.status;
					            		value.oozie_realtime = false;
					            	}
					            	return value;
					            });
					            this.setState({ data: data });
					            this.setState({ statusFailingTime: ++this.state.statusFailingTime });
					        }
					    } else {
							const data = this.state.data;
								if (Array.isArray(data)) data.map((value, index, array) => {
									if (value.oozie_job_id === action.data.oozie_job_id) {
										value.oozie_status = action.status;
					            		value.oozie_realtime = false;
									}
								return value;
							});
							this.setState({ data: data });
							this.setState({ statusFailingTime: ++this.state.statusFailingTime });
					    }
					    if (this.state.statusFailingTime >= this.state.data.length) {
					    	this.props.showAlert({ 
					    		style: `danger`, 
					    		title: `Failed to Load Real-Time Status of ALL Oozie Jobs!`, 
					    		message: `We are not capable to fetch the status data of ALL Oozie Jobs at this moment. It looks like Oozie server couldn't be contacted right now.`
					    	});
					    	this.setState({ realtime: false });
					    } else if (fails) {
					    	this.props.showAlert({ 
					    		style: `warning`, 
					    		title: `Warning: Some of oozie jobs' status are NOT realtime!`, 
					    		message: `Number of realtime failed status: ${ this.state.statusFailingTime } of ${ this.state.data.length } oozie job(s).`
					    	});
					    }
			      	}, action.data.oozie_job_id);
		    	}
		    	break;
			case "VIEW_OOZIE_DETAILS":
				this.props.onPageChange("oozie-details", { oozie: action.data });
				break;
			default: return;
		}
	}

	oozieJobIdFormatter = (cell, row) => <strong>{ cell }</strong>;

	statusFormatter = (cell, row) => (row.oozie_realtime) ? (
		<strong>{ cell }</strong>
	) : (
		<div>
			{ cell } <strong><font color="red">[NOT REALTIME]</font></strong>
		</div>
	);

	actionsFormatter = (cell, row) => (
		<div>
			<Button
				bsStyle="info" className="btn-mini"
				onClick={ () => this.dispatchAction({ type: "VIEW_OOZIE_DETAILS", data: row }) } >
				View Oozie Details
			</Button>
		</div>
	);

	render() {
		const tableOptions = {
			noDataText: ("undefined" === typeof this.props.project || null === this.props.project) ? (<center>It seems that you don't have any oozie job recorded at the moment.</center>) : (<center>It seems that project { this.props.project.project_name } doesn't have any oozie job recorded at the moment.</center>),
		};
		return (
			<div id="oozie-content">
				<div className="row-fluid">
					<div className="span12">
						<div style={{ padding: "5px 0px 0px 0px" }}>
							<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_OOZIE" }) }>
								Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
							</Button>
							&nbsp;
							{ 
								( this.state.realtime ) ? (
									<Button bsStyle="success"
										onClick={ () => {
											this.setState({ realtime: false });
											this.dispatchAction({ type: "LOAD_OOZIE" });
										}}>
										Real-Time Status: ON
									</Button>
								) : (
									<Button bsStyle="danger" 
										onClick={ () => {
											this.setState({ realtime: true });
											this.dispatchAction({ type: "LOAD_OOZIE" });
										}}>
										Real-Time Status: OFF
									</Button>
								) 
							}
						</div>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>{ ("undefined" === typeof this.props.group || null === this.props.group) ? "My Oozie Job List" : "Oozie Job History of Project " + this.props.project.project_name }</h5>
					      	</div>
							<div>
							    <BootstrapTable data = { this.state.pageOfData } options = { tableOptions } >
							        <TableHeaderColumn 
							        	dataField = "oozie_job_id" isKey
							        	headerAlign = "center" dataSort dataFormat = { this.oozieJobIdFormatter } >
							        	Oozie Job ID
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "oozie_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Created At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "oozie_create_date"
							        	headerAlign = "center" dataAlign = "center" dataSort >
							        	Updated At
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "oozie_status"
							        	headerAlign = "center" dataAlign = "center" dataSort dataFormat = { this.statusFormatter } >
							        	Status
							        </TableHeaderColumn>
							        <TableHeaderColumn
							        	dataField = "oozie_actions"
							        	headerAlign = "center" dataAlign = "center" dataFormat = { this.actionsFormatter } >
							        	Actions
							        </TableHeaderColumn>
							    </BootstrapTable>
							    <Pagination className="pull-right" items={this.state.data} onChangePage={
				      				(pageOfData: Array<any>) => { 
				      					this.dispatchAction({
				      						type: "PAGE_CHANGE",
				      						pageOfData: pageOfData,
				      					}); 
				      				}
				      			} />
			      			</div>
			    		</div>
			  		</div>
				</div>
			</div>
		);
	}

}