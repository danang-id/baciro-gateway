import React from 'react';
import { Button, Form, FormControl, Modal, ToggleButton, ToggleButtonGroup } from 'react-bootstrap';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import Pagination from './../../../generic/Pagination';
import OozieService from './../../../../services/OozieService';

export default class OozieDetailsContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: any;

	constructor(props) {
		super(props);
		this.state = {
			data: [],
			pageOfData: [],
		};
	}

	componentDidMount = () => {
		this.dispatchAction({ type: "LOAD_OOZIE_DETAILS" });
		this.setState({ mounted: true });
	}
	componentDidUpdate = (prevProps, prevState) => ( this.props.project !== prevProps.project ) ? this.dispatchAction({ type: "LOAD_OOZIE_DETAILS" }) : null;
	componentWillUnmount = () => {
		this.setState({ mounted: false });
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "PAGE_CHANGE":
				this.setState({ pageOfData: action.pageOfData }); 
				break;
			case "LOAD_OOZIE_DETAILS":
				const notReady = (
					<center>
						<br></br>
						<strong>Implementation is not available at the moment. Details of this Oozie Job couldn't be shown.</strong><br></br>
						<br></br>
					</center>
				); 
				this.props.showAlert({
					style: `danger`, 
					title: `Details of Oozie Jobs [${ this.props.oozie.oozie_job_id }] could NOT be shown!`, 
					message: `The implementation of this content is not avaiable at the moment. Please come back soon.`
				});
				this.setState({ data: notReady });
		      	break;
			default: return;
		}
	}

	render() {
		return (
			<div id="oozie-details-content">
				<div className="row-fluid">
					<div className="span12">
						<div className="pull-right"  style={{ padding: "5px 0px 10px 0px" }}>
							<Button bsStyle="info" onClick={ () => this.dispatchAction({ type: "LOAD_OOZIE_DETAILS" }) }>
								Refresh Data <span className="plus-link"><i className="icon-refresh"></i></span>
							</Button>
						</div>
					</div>
				</div>
				<div className="row-fluid">
			  		<div className="span12">
			    		<div className="widget-box">
			    			<div className="widget-title">
					        	<span className="icon"> <i className="icon-align-justify"></i> </span>
					        	<h5>Details of Oozie Job [{ this.props.oozie.oozie_job_id }]</h5>
					      	</div>
							<div>
							    { this.state.data }
			      			</div>
			    		</div>
			  		</div>
				</div>
			</div>
		);
	}

}