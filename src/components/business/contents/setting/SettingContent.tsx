import React from 'react';
import { Button, Form, FormControl, Modal } from 'react-bootstrap';
import 'react-bootstrap-table/dist/react-bootstrap-table-all.min.css';

import ClusterService from './../../../../services/ClusterService';
import UserManagementService from './../../../../services/UserManagementService';

export default class UserContent extends React.Component {
	
	private state: any;
	private props: any;
	private setState: Function;

	constructor(props) {
		super(props);
		this.state = {
			data: {
				user_firstname: `Getting data...`, 
				user_lastname: `Getting data...`, 
				user_email: `Getting data...`,  
				cluster_id: `Getting data...`, 
				cluster_name: `Getting data...`, 
			},
			modal: {
			 	useraccount: { show: false, data: {} },
			 	password: { show: false, data: {} },
			 	cluster: { show: false, data: {} },
			},
		};
	}

	componentWillMount = () => this.dispatchAction({ type: "LOAD_SETTING" });

	modalHandler(modalName: string, isShown: boolean = false, data: object = {}) {
		switch (modalName) {
			case "useraccount":
				this.setState({
					modal: {
					 	useraccount: { show: isShown, data: data },
					 	password: { show: false, data: {} },
					 	cluster: { show: false, data: {} },
					}
				});
				break;
			case "password":
				this.setState({
					modal: {
					 	useraccount: { show: false, data: {} },
					 	password: { show: isShown, data: data },
					 	cluster: { show: false, data: {} },
					}
				});
				break;
			case "cluster":
				this.setState({
					modal: {
					 	useraccount: { show: false, data: {} },
					 	password: { show: false, data: {} },
					 	cluster: { show: isShown, data: data },
					}
				});
				break;
			default:
				break;
		}
	}

	dispatchAction = (action: any) => {
		if (action === void 0) return;
		switch (action.type) {
			case "LOAD_SETTING": 
				/**
				 * TODO:
				 * Check: already using `user_cluster_id` variable to set Cluster is Use
				 */
				UserManagementService.getUser((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = feedback.data[0];
				            	if (`null` !== data.user_cluster_id) {
				            		this.dispatchAction({ type: "LOAD_CLUSTER", data: data })
				            	} else {
				            		data.cluster_id = `default`;
				            		data.cluster_name = `Using Default Cluster`
				            		this.setState({ data: data });
				            	}
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Load User Setting!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	}, this.props.credentials.user_id);
		      	break;
			case "LOAD_CLUSTER": 
				ClusterService.getCluster((success: boolean, request: Request, response: any) => {
					const data = action.data;
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	data.cluster_id = feedback.data[0].cluster_id;
				            	data.cluster_name = feedback.data[0].cluster_name;
				            } else {
				            	data.cluster_id = `null`;
				            	data.cluster_name = `Data is not available at the moment.`
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Get List of Cluster!`, message: `${error}`});
				            }
				        } catch (exception) {
				            data.cluster_id = `null`;
				            data.cluster_name = `Data is not available at the moment.`
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	data.cluster_id = `null`;
				        data.cluster_name = `Data is not available at the moment.`
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
				    this.setState({ data: data });
		      	}, action.data.user_cluster_id);
		      	break;
			case "EDIT_USERACCOUNT":
				this.modalHandler("useraccount", true, action.data);
				break;
			case "EDIT_PASSWORD":
				this.modalHandler("password", true, action.data);
				break;
			case "EDIT_CLUSTER":
				ClusterService.getCluster((success: boolean, request: Request, response: any) => {
		      		if (success) {
		        		try {
		        			const feedback = JSON.parse(response);
				            if (feedback.err_code === 0) {
				            	const data = [];
				            	if (Array.isArray(data)) data.push({
				            		cluster_id: `null`,
				            		cluster_name: `Default Cluster`
				            	});
				            	if (Array.isArray(feedback.data)) feedback.data.map((value, index, array) => data.push(value));
				            	this.modalHandler("cluster", true, data);
				            } else {
				            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
				            	this.props.showAlert({ style: `danger`, title: `Failed to Get List of Cluster!`, message: `${error}`});
				            }
				        } catch (exception) {
				        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				        }
				    } else {
				    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
				    }
		      	});
				break;
			case "UPDATE_USERACCOUNT":
				this.modalHandler("useraccount");
				if (action.confirmed) {
					const formElement = document.getElementById("form-useraccount-edit");
					const formData = new FormData(formElement);
					UserManagementService.putUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.onCredentialsChange(feedback.data[0]);
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `User Account Updated Successfully!`,
					            		message: `User Account has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update User Account!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_SETTING" });
			      	}, formData, this.props.credentials.user_id);
				} 
				break;
			case "UPDATE_PASSWORD":
				this.modalHandler("password");
				if (action.confirmed) {
					const formElement = document.getElementById("form-password-edit");
					const formData = new FormData(formElement);
					UserManagementService.putUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Password Updated Successfully!`,
					            		message: `Your password has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Your Password!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_SETTING" });
			      	}, formData, this.props.credentials.user_id);
				} 
				break;
			case "UPDATE_CLUSTER":
				this.modalHandler("cluster");
				if (action.confirmed) {
					const formElement = document.getElementById("form-cluster-edit");
					const formData = new FormData(formElement);
					UserManagementService.putUser((success: boolean, request: Request, response: any) => {
			      		if (success) {
			        		try {
			        			const feedback = JSON.parse(response);
					            if (feedback.err_code === 0) {
					            	this.props.onCredentialsChange(feedback.data[0]);
					            	this.props.showAlert({ 
					            		style: `success`, 
					            		title: `Cluster Configuration Updated Successfully!`,
					            		message: `Your cluster configuration has been successfully updated.`
					            	});
					            } else {
					            	const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
					            	this.props.showAlert({ 
					            		style: `danger`, 
					            		title: `Failed to Update Cluster Configuration!`,
					            		message: `${error}`}
					            	);
					            }
					        } catch (exception) {
					        	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					        }
					    } else {
					    	this.props.showAlert({ style: `danger`, title: `Oops! Something is not right...`, message: `${response}`});
					    }
					    this.dispatchAction({ type: "LOAD_SETTING" });
			      	}, formData, this.props.credentials.user_id);
				} 
				break;
			default:
				return;
		}
	}

	renderClusterList = () => {
		const clusters = ("undefined" !== typeof this.state.modal.cluster.data) ? this.state.modal.cluster.data : [];
		const options = [];
		if (Array.isArray(clusters)) clusters.map((value, index, array) => {
			return options.push(
				<option key={ value.cluster_id } value={ value.cluster_id }>
					{ value.cluster_name } [Cluster-ID: { value.cluster_id }]
				</option>);
		});
		return options;
	}

	render() {
		return (
			<div id="setting-content">
				<div className="row-fluid">
					<div className="span6">
					    <div className="widget-box">
					      	<div className="widget-title">
					        	<span className="icon"> <i className="icon-user"></i> </span>
					        	<h5> User Account </h5>
					        	<a onClick={ () => this.dispatchAction({ type: "EDIT_USERACCOUNT", data: this.state.data }) } title="Edit User Account" className="pull-right tip-bottom"><i style={{ margin: "5px" }} className="pull-right icon-edit" ></i> </a>
					      	</div>
					      	<div>
					        	<table className="table table-user-information">
					          		<tbody>
					            		<tr>
					            			<td>First Name</td>
							              	<td id='user_firstname'>
							              		{ this.state.data.user_firstname }
							              	</td>
					            		</tr>
							            <tr>
								            <td>Last Name</td>
								            <td id='user_lastname'>
								            	{ this.state.data.user_lastname }
								            </td>
							            </tr>
							            <tr>
							            	<td>Email Address</td>
							            	<td id='user_email'>
							            		{ this.state.data.user_email }
							            	</td>
							            </tr>
							            <tr>
							            	<td>User Role</td>
							            	<td id='user_role_name'>
							            		{ (this.state.data.user_role_id === 1) ? "Administrator" : (this.state.data.user_role_id === 2) ? "Operator" : "Getting data..." }
							            	</td>
							            </tr>
						          	</tbody>
						        </table>
						    </div>
						</div>
					</div>
					<div className="span6">
					    <div id="widget-box-cluster-configuration" className="widget-box">
					      	<div className="widget-title">
					        	<span className="icon"> <i className="icon-user"></i> </span>
					        	<h5> Cluster Configuration </h5>
					        	<a onClick={ () => this.dispatchAction({ type: "EDIT_CLUSTER" }) } title="Edit Cluster Configuration" className="pull-right tip-bottom"><i style={{ margin: "5px" }} className="pull-right icon-edit" ></i> </a>
					      	</div>
					      	<div>
					        	<table className="table table-user-information">
					          		<tbody>
					            		<tr>
					              			<td>Cluster in Use</td>
					              			<td id='cluster_name'>
					              				{ this.state.data.cluster_name }
					              			</td>
					            		</tr>
					          		</tbody>
					        	</table>
					      	</div>
					    </div>
					</div>
				</div>
				<Modal show = { this.state.modal.useraccount.show } onHide = { () => { this.modalHandler("useraccount") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>User Account</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-useraccount-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_USERACCOUNT", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.modal.useraccount.data.user_id } />
							<div className="control-group">
								<label className="control-label">First Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_firstname"
										placeholder="First Name" 
										defaultValue={ this.state.modal.useraccount.data.user_firstname }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Last Name:</label>
								<div className="controls">
									<FormControl 
										type="text"
										name="user_lastname"
										placeholder="Last Name" 
										defaultValue={ this.state.modal.useraccount.data.user_lastname }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Email Address:</label>
								<div className="controls">
									<FormControl 
										type="email"
										name="user_email"
										placeholder="someone@solusi247.com" 
										defaultValue={ this.state.modal.useraccount.data.user_email }
										required />
								</div>
							</div>
							<div className="control-group">
								<label className="control-label">Password:</label>
								<div className="controls">
									<Button
										bsStyle="danger"
										onClick={ () => { 
											const row = this.state.modal.useraccount.data;
											this.dispatchAction({type: "UPDATE_USERACCOUNT", confirmed: false});
											this.dispatchAction({ type: "EDIT_PASSWORD", data: row });
										} } >
										Change Password
									</Button>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_USERACCOUNT", confirmed: true}) } >
							Update
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_USERACCOUNT", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.password.show } onHide = { () => { this.modalHandler("password") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Change Your Password</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-password-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_PASSWORD", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.modal.password.data.user_id } />
							<div className="control-group">
								<label className="control-label">New Password:</label>
								<div className="controls">
									<FormControl 
										type="password"
										name="user_password"
										placeholder="Something Secret" 
										required />
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_PASSWORD", confirmed: true}) } >
							Change Password
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_PASSWORD", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
				<Modal show = { this.state.modal.cluster.show } onHide = { () => { this.modalHandler("cluster") } }>
					<Modal.Header closeLabel="" closeButton>
						<Modal.Title>Cluster Configuration</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form
							id = "form-cluster-edit"
							onKeyPress = { (e) => {
								if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
									e.preventDefault();
									this.dispatchAction({type: "UPDATE_CLUSTER", confirmed: true})
								}
							} } 
							horizontal>
							<FormControl 
								type="hidden"
								name="user_id" 
								defaultValue={ this.state.data.user_id } />
							<div className="control-group">
								<label className="control-label">Set Cluster:</label>
								<div className="controls">
									<FormControl 
										componentClass="select"
										name="user_cluster_id" >
										{ this.renderClusterList() }
									</FormControl>
								</div>
							</div>
						</Form>
					</Modal.Body>
					<Modal.Footer>
						<Button
							bsStyle="primary"
							onClick={ () => this.dispatchAction({type: "UPDATE_CLUSTER", confirmed: true}) } >
							Save this Cluster as My Cluster 
						</Button>
						<Button
							bsStyle="danger"
							onClick={ () => this.dispatchAction({type: "UPDATE_CLUSTER", confirmed: false}) } >
							Cancel
						</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

}