import React from 'react';
import { Navbar } from 'react-bootstrap';

const Footer = (props: any) => {
	return (
		<Navbar className="footer" fixedBottom>
			{ props.appName } &copy; { (new Date()).getFullYear() }.
			Brought to you by <a href={ props.providerUrl }>{ props.providerName }</a>.
		</Navbar>
	);
}

export default Footer;