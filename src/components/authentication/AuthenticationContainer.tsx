import React from 'react';
import { Alert } from 'react-bootstrap';
import FormData from 'formdata-polyfill';

import SignInForm from './SignInForm';
import RegistrationModal from './RegistrationModal';

import ApplicationBase from './../../services/ApplicationBase';
import AuthenticationService from './../../services/AuthenticationService';

export default class AuthenticationContainer extends React.Component {

    private base: ApplicationBase;
    private props: any;
  	private state: any;

    constructor(props: any) {
        super(props);
        this.base = new ApplicationBase();
        this.state = { 
          registrationAlert: null,
          registrationFormId: 'form-registration',
          showRegistrationModal: false,
          signInAlert: null,
          signInFormId: 'form-sign-in',
        };
    }

    componentWillMount() {
      this.base.splash.show();
    }

    componentDidMount() {
      this.base.splash.hide();
    }

    showAlert(stage: string, title: string, message: String, style: string = `default`) {
      let onDismiss;
      if (stage === `signin`) onDismiss = { signInAlert: null };
      else if (stage === `registration`) onDismiss = { registrationAlert: null };
      const alert = (
        <Alert bsStyle={ style } onDismiss={ () => this.setState(onDismiss) } closeLabel="" >
          <strong>{ title }</strong>
          <p>{ message }</p>
        </Alert>
      );
      if (stage === `signin`) this.setState({ signInAlert: alert });
      else if (stage === `registration`) this.setState({ registrationAlert: alert });
    }

    handleSignIn() {
      const formElement = document.getElementById(this.state.signInFormId);
      const formData = new FormData(formElement);
      AuthenticationService.signIn((operationSuccess: boolean, request: Request, response: any) => {
        if (operationSuccess) {
          try {
            const feedback = JSON.parse(response);
            if (feedback.err_code === 0) {
              this.base.cookies.put('credentials', feedback.data);
              this.props.onCredentialsChange(feedback.data);
            } else {
              const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
              this.showAlert(`signin`, `Sign-In Failed!`, `${error}`, `danger`);
            }
          } catch (exception) {
              this.showAlert(`signin`, `Sign-In Failed!`, `${response}`, `danger`);
          }
        } else {
          this.showAlert(`signin`, `Sign-In Failed!`, `${response}`, `danger`);
        }
      }, formData);
    }

    handleRegistration() {
      const formElement = document.getElementById(this.state.registrationFormId);
      const formData = new FormData(formElement);
      AuthenticationService.signUp((operationSuccess: boolean, request: Request, response: any) => {
        if (operationSuccess) {
          try {
            const feedback = JSON.parse(response);
            if (feedback.err_code === 0) {
              this.handleRegistrationModalClose();
              this.showAlert(`signin`, `User Registration Success!`, `${feedback.data.user_email} has been successfully registered to the system. You may sign in now...`, `success`);
            } else {
              const error = feedback.err_msg ? feedback.err_msg : `Unknown error.`;
              this.showAlert(`registration`, `User Registration Failed!`, `${error}`, `danger`);
            }
          } catch (exception) {
            this.showAlert(`registration`, `User Registration Failed!`, `${response}`, `danger`);
          }
        } else {
          this.showAlert(`registration`, `User Registration Failed!`, `${response}`, `danger`);
        }
      }, formData);
    }

    handleRegistrationModalOpen() {
      this.setState({ showRegistrationModal: true  })
    }

    handleRegistrationModalClose() {
      this.setState({ showRegistrationModal: false  })
    }

    render() {
      return (
        <div id="authentication-container">
          <SignInForm 
            alert={this.state.signInAlert}
            formId={this.state.signInFormId}
            onSignIn={this.handleSignIn.bind(this)}
            onRegister={this.handleRegistrationModalOpen.bind(this)}
          />
          <RegistrationModal 
            alert={this.state.registrationAlert}
            formId={this.state.registrationFormId}
            onClose={this.handleRegistrationModalClose.bind(this)}
            onRegister={this.handleRegistration.bind(this)}
            shown={this.state.showRegistrationModal} 
          />
        </div>
      );
    }
}