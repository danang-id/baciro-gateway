import React from 'react';  
import { Alert, Button, Form, FormControl, FormGroup, Glyphicon, Image, Modal } from 'react-bootstrap';

const SignInForm = (props) => {
  return (
      <div id="loginbox">
        <Form id={props.formId} onKeyPress = { (e) => {
                if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
                  e.preventDefault();
                  props.onSignIn();
                }
              } } >
          <div className="control-group normal_text">
            <Image src={ `${process.env.PUBLIC_URL}/static/img/baciro-logo.png` } responsive={true} alt="Baciro Gateway" style={{ width: "70%" }} />
          </div>
          <div className="control-group">
            {props.alert}
          </div>
          <div className="control-group">
            <div className="controls">
              <div className="main_input_box">
                <span className="add-on bg_lg"><i className="icon-user" /></span>
                <FormControl
                  type="email"
                  placeholder="someone@solusi247.com"
                  name="user_email"
                  required
                />
              </div>
            </div>
          </div>
          <div className="control-group">
            <div className="controls">
              <div className="main_input_box">
                <span className="add-on bg_ly"><i className="icon-lock" /></span>
                <FormControl
                  type="password"
                  placeholder="Password"
                  name="user_password"
                  required
                />
              </div>
            </div>
          </div>
          <div className="form-actions">
            <div className="controls pull-right">
              <Button bsStyle="success" onClick={ props.onSignIn } style={{padding:"5px 30px", borderRadius: "4px" }}>Sign In</Button> <Button bsStyle="default" onClick={ props.onRegister } style={{padding:"5px 30px", borderRadius: "4px" }}>Register</Button>
            </div>
          </div>
        </Form>
      </div>
  );
}

export default SignInForm;