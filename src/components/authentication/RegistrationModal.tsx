import React from 'react';  
import { Alert, Button, Form, FormControl, FormGroup, Modal } from 'react-bootstrap';

const RegistrationModal = (props) => {
  return (
    <Modal show={props.shown} onHide={props.onClose}>
      <Modal.Header closeLabel="" closeButton>
        <Modal.Title>User Registration</Modal.Title>
      </Modal.Header>
      <Modal.Body className="form">
        <Form id={props.formId} 
          onKeyPress = { (e) => {
            if ((e.which && e.which === 13) || (e.keyCode && e.keyCode === 13)) {
              e.preventDefault();
              props.onRegister();
            }
          } } 
          horizontal>
                <div className="control-group">
                  {props.alert}
                </div>
                <div className="control-group">
                  <label className="control-label">First Name</label>
                  <div className="controls">
                    <FormControl type="text" placeholder="First Name" name="user_firstname" required />
                  </div>
                </div>
                <div className="control-group">
                  <label className="control-label">Last Name</label>
                  <div className="controls">
                    <FormControl type="text" placeholder="Last Name" name="user_lastname" required />
                  </div>
                </div>
                <div className="control-group">
                  <label className="control-label">E-mail Address</label>
                  <div className="controls">
                    <FormControl type="email" placeholder="someone@solusi247.com" name="user_email" required />
                  </div>
                </div>
                <div className="control-group">
                  <label className="control-label">Password</label>
                  <div className="controls">
                    <FormControl type="password" placeholder="Type a password..." name="user_password" minLength="6" pattern=".{6,}" required />
                  </div>
                </div>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        <Button bsStyle="primary" onClick={props.onRegister}>Register</Button>
        <Button bsStyle="danger" onClick={props.onClose}>Close</Button>
      </Modal.Footer>
    </Modal>
  );
}

export default RegistrationModal;